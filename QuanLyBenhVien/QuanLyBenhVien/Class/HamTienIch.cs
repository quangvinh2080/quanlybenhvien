﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QuanLyBenhVien
{
    public class HamTienIch
    {
        public static string htiChuyenDoiMa1KyTu(string maCuoi, string kyTuDauMaMoi)
        {
            int soMaKhoa = Convert.ToInt32(maCuoi.Substring(1, 4)) + 1;
            string maKhoaMoi = "";
            int donVi = soMaKhoa / 10;
            if (donVi == 0)
            {
                maKhoaMoi = kyTuDauMaMoi + "000" + soMaKhoa;
            }
            else if (donVi > 0 && donVi < 10)
            {
                maKhoaMoi = kyTuDauMaMoi + "00" + soMaKhoa;
            }
            else if (donVi >= 10 && donVi < 100)
            {
                maKhoaMoi = kyTuDauMaMoi + "0" + soMaKhoa;
            }
            else
            {
                maKhoaMoi = kyTuDauMaMoi + soMaKhoa;
            }
            return maKhoaMoi;
        }

        public static string htiChuyenDoiMa2KyTu(string maCuoi, string kyTuDauMaMoi)
        {
            int soMaKhoa = Convert.ToInt32(maCuoi.Substring(2, 3)) + 1;
            string maKhoaMoi = "";
            int donVi = soMaKhoa / 10;
            if (donVi == 0)
            {
                maKhoaMoi = kyTuDauMaMoi + "00" + soMaKhoa;
            }
            else if (donVi > 0 && donVi < 10)
            {
                maKhoaMoi = kyTuDauMaMoi + "0" + soMaKhoa;
            }
            else
            {
                maKhoaMoi = kyTuDauMaMoi + soMaKhoa;
            }
            return maKhoaMoi;
        }
    }
}
