﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.XtraGrid.Localization;

namespace QuanLyBenhVien
{
    public class MyGridLocalizer : GridLocalizer
    {
        public override string GetLocalizedString(GridStringId id)
        {
            if (id == GridStringId.FindControlFindButton)
                return "Tìm kiếm";
            if (id == GridStringId.FindControlClearButton)
                return "Xoá";
            return base.GetLocalizedString(id);
        }
    }
}
