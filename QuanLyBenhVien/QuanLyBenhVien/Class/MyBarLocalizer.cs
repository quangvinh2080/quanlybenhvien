﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.XtraBars.Localization;

namespace QuanLyBenhVien
{
    public class MyBarLocalizer : BarLocalizer
    {
        public override string GetLocalizedString(BarString id)
        {
            if (id == BarString.SkinCaptions)
            {
                //Default value for BarString.SkinCaptions:
                //"|DevExpress Style|Caramel|Money Twins|DevExpress Dark Style|iMaginary|Lilian|Black|Blue|Office 2010 Blue|Office 2010 Black|Office 2010 Silver|Office 2007 Blue|Office 2007 Black|Office 2007 Silver|Office 2007 Green|Office 2007 Pink|Seven|Seven Classic|Darkroom|McSkin|Sharp|Sharp Plus|Foggy|Dark Side|Xmas (Blue)|Springtime|Summer|Pumpkin|Valentine|Stardust|Coffee|Glass Oceans|High Contrast|Liquid Sky|London Liquid Sky|The Asphalt World|Blueprint|"
                string defaultSkinCaptions = base.GetLocalizedString(id);
                string newSkinCaptions = defaultSkinCaptions.Replace("|DevExpress Style|", "|My Favorite Skin|");
                return newSkinCaptions;
            }
            return base.GetLocalizedString(id);
        }

    }
}
