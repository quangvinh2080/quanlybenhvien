﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using BLL;

namespace QuanLyBenhVien
{
    public partial class frmDangNhap : DevExpress.XtraEditors.XtraForm
    {
        
        public frmDangNhap()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Đăng nhập hệ thống
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mbtnDangNhap_Click(object sender, EventArgs e)
        {
            //kiểm tra tên đăng nhập đúng hay sai
            //giả sử tên đăng nhập đúng
            MessageBox.Show("Bạn đã đăng nhập thành công!","Quản lý bệnh viện",MessageBoxButtons.OK,MessageBoxIcon.Information);
            BienToanCuc.maNhanVien = "";//ma nhan vien
            BienToanCuc.tenNhanVien = "Thiều Quang Vinh";
            BienToanCuc.maNghiepVu = "NV001";
            mtxtUser.Text = mtxtPassword.Text = string.Empty;
            QuanLyBenhVien.frmMainpage.pfrmInstance.mxtraTabControl.TabPages.RemoveAt(1);
            //trạng thái button cơ bản
            QuanLyBenhVien.frmMainpage.pfrmInstance.mpThayDoiTrangThaiDangNhap();
            //

            //Kiểm tra phân quyền
            //Admin : All
            //Bác sĩ: Khám bệnh, Quản lý thuốc
            //Nhân viên y tế: Tiếp nhận
            //Thu ngân: Thu Phí
            //

            //Phân quyền admin
            dnPhanQuyenAdmin();
            //
            //Phân quyền bác sĩ
            //dnPhanQuyenBacSi();
            //
            //Phân quyền nhân viên y tế
            //dnPhanQuyenTiepNhan();
            //
            //Phân quyền thu ngân
            //dnPhanQuyenThuNgan();
            //
            frmWelcome frm_Welcome = new frmWelcome();
            DieuKhien.dkTaoTab(QuanLyBenhVien.frmMainpage.pfrmInstance.frm_Tabmain.xtraTabControl1, "UIT-hospital", frm_Welcome.Name, frm_Welcome);
        }

        private void dnPhanQuyenThuNgan()
        {
            QuanLyBenhVien.frmMainpage.pfrmInstance.mpThayDoiTenNguoiDung("<Thu phí>"+BienToanCuc.tenNhanVien);
            QuanLyBenhVien.frmMainpage.pfrmInstance.mpTrangThaiButton("mbtnThuPhi", true);
        }

        private void dnPhanQuyenTiepNhan()
        {
            QuanLyBenhVien.frmMainpage.pfrmInstance.mpThayDoiTenNguoiDung("<Tiếp nhận>"+BienToanCuc.tenNhanVien);
            QuanLyBenhVien.frmMainpage.pfrmInstance.mpTrangThaiButton("mbtnTiepNhan", true);
        }

        private void dnPhanQuyenBacSi()
        {
            QuanLyBenhVien.frmMainpage.pfrmInstance.mpThayDoiTenNguoiDung("<Bác sĩ>"+BienToanCuc.tenNhanVien);
            QuanLyBenhVien.frmMainpage.pfrmInstance.mpTrangThaiButton("mbtnKhamBenh", true);
        }

        private void dnPhanQuyenAdmin() 
        {
            QuanLyBenhVien.frmMainpage.pfrmInstance.mpThayDoiTenNguoiDung("<Administrator>"+BienToanCuc.tenNhanVien);
            QuanLyBenhVien.frmMainpage.pfrmInstance.mpThayDoiTrangThaiTatCaButton(true);
        }
    }
}