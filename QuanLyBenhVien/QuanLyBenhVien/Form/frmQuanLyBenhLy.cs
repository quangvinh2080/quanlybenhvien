﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using BLL;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Columns;

namespace QuanLyBenhVien
{
    public partial class frmQuanLyBenhLy : DevExpress.XtraEditors.XtraForm
    {
        public BangBenhLy bbl = new BangBenhLy();
        public static frmQuanLyBenhLy pqlblInstance;
        public frmQuanLyBenhLy()
        {
            InitializeComponent();
            pqlblInstance = this;
        }

        private void frmQuanLyBenhLy_Load(object sender, EventArgs e)
        {
            gridControl1.DataSource = BangBenhLy.bblLayThongTin();
            mgvBenhLy.Columns[0].FieldName = "MaBenh";
            mgvBenhLy.Columns[1].FieldName = "TenBenh";
            mgvBenhLy.Columns[2].FieldName = "MaICD";
            mgvBenhLy.Columns[3].FieldName = "MaNhomBenh";

            repositoryItemLookUpEdit1.DataSource = BangNhomBenh.bnbLayThongTin();
            repositoryItemLookUpEdit1.DisplayMember = "TenNhomBenh";
            repositoryItemLookUpEdit1.ValueMember = "MaNhomBenh";
        }

        private void btnThemNhom_Click(object sender, EventArgs e)
        {
            frmThemNhomBenh frm_ThemNhomBenh = new frmThemNhomBenh();
            frm_ThemNhomBenh.ShowDialog();
        }

        private void mgvBenhLy_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            string maBenh = "";
            if (mgvBenhLy.RowCount == 0)
                maBenh = "L0001";
            else
            {
                maBenh = HamTienIch.htiChuyenDoiMa1KyTu(BangBenhLy.bblLayMaBenhCuoi(), "L");
            }
            ColumnView View = sender as ColumnView;
            View.SetRowCellValue(e.RowHandle, View.Columns["MaBenh"], maBenh);
        }

        private void mgvBenhLy_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            if (e.RowHandle < 0)
            {
                bbl.bblThemBenh(mgvBenhLy.GetFocusedRowCellValue("MaBenh").ToString(), mgvBenhLy.GetFocusedRowCellValue("TenBenh").ToString(), mgvBenhLy.GetFocusedRowCellValue("MaICD").ToString(), mgvBenhLy.GetFocusedRowCellValue("MaNhomBenh").ToString());
                XtraMessageBox.Show("Thêm bệnh lý thành công");
            }
            else
            {
                bbl.bblSuaBenh(mgvBenhLy.GetFocusedRowCellValue("MaBenh").ToString(), mgvBenhLy.GetFocusedRowCellValue("TenBenh").ToString(), mgvBenhLy.GetFocusedRowCellValue("MaICD").ToString(), mgvBenhLy.GetFocusedRowCellValue("MaNhomBenh").ToString());
                XtraMessageBox.Show("Cập nhật thành công");
            }
        }

        private void txtTimKiem_TextChanged(object sender, EventArgs e)
        {
            if (txtTimKiem.Text != "")
            {
                switch (rdgTimTheo.Text)
                {
                    case "name":
                        mgvBenhLy.ApplyFindFilter("Tên:\"" + txtTimKiem.Text + "\"");
                        break;
                    case "group":
                        mgvBenhLy.ApplyFindFilter("Nhóm:\"" + txtTimKiem.Text + "\"");
                        break;
                    case "maicd":
                        mgvBenhLy.ApplyFindFilter("\"Mã ICD\":\"" + txtTimKiem.Text + "\"");
                        break;
                }
            }
            else
            {
                mgvBenhLy.ApplyFindFilter("");
            }
        }

        private void mgvBenhLy_ValidateRow(object sender, ValidateRowEventArgs e)
        {
        //    GridView view = sender as GridView;
        //    if (view.IsNewItemRow(e.RowHandle)) 
        //    { 
        //        GridColumn column = view.Columns["TenBenh"];
        //        object val = view.GetRowCellValue(e.RowHandle, column);
        //        if (val == null || val.ToString() == string.Empty)
        //        {
        //            e.Valid = false;
        //            view.SetColumnError(column, "Tên bệnh không thể rỗng");
        //        }
        //    }
        }
    }
}