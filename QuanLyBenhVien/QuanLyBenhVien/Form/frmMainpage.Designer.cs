﻿namespace QuanLyBenhVien
{
    partial class frmMainpage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMainpage));
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.mbtnTiepNhan = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem5 = new DevExpress.XtraBars.BarButtonItem();
            this.mbtnThuoc = new DevExpress.XtraBars.BarButtonItem();
            this.mbtnTtb = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem8 = new DevExpress.XtraBars.BarButtonItem();
            this.mbtnQuanLyNS = new DevExpress.XtraBars.BarButtonItem();
            this.mbtnDangNhap = new DevExpress.XtraBars.BarButtonItem();
            this.mbtnDangXuat = new DevExpress.XtraBars.BarButtonItem();
            this.mbtnThayDoiMK = new DevExpress.XtraBars.BarButtonItem();
            this.mbtnBaoCaoTon = new DevExpress.XtraBars.BarButtonItem();
            this.mbtnCauHinh = new DevExpress.XtraBars.BarButtonItem();
            this.mtxtTenNhanVien = new DevExpress.XtraBars.BarStaticItem();
            this.ribbonGalleryBarItem1 = new DevExpress.XtraBars.RibbonGalleryBarItem();
            this.mbtnKhamBenh = new DevExpress.XtraBars.BarButtonItem();
            this.mbtnThuPhi = new DevExpress.XtraBars.BarButtonItem();
            this.mbtnQuanLyBL = new DevExpress.XtraBars.BarButtonItem();
            this.mbtnQuanLyVP = new DevExpress.XtraBars.BarButtonItem();
            this.mbtnQuanLyCLS = new DevExpress.XtraBars.BarButtonItem();
            this.mbtnBaoCaoDoanhThu = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup7 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup8 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup5 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup6 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage3 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup4 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup9 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup10 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup11 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage2 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.mxtraTabControl = new DevExpress.XtraTab.XtraTabControl();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mxtraTabControl)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.mbtnTiepNhan,
            this.barButtonItem3,
            this.barButtonItem4,
            this.barButtonItem5,
            this.mbtnThuoc,
            this.mbtnTtb,
            this.barButtonItem8,
            this.mbtnQuanLyNS,
            this.mbtnDangNhap,
            this.mbtnDangXuat,
            this.mbtnThayDoiMK,
            this.mbtnBaoCaoTon,
            this.mbtnCauHinh,
            this.mtxtTenNhanVien,
            this.ribbonGalleryBarItem1,
            this.mbtnKhamBenh,
            this.mbtnThuPhi,
            this.mbtnQuanLyBL,
            this.mbtnQuanLyVP,
            this.mbtnQuanLyCLS,
            this.mbtnBaoCaoDoanhThu});
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 30;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.PageHeaderItemLinks.Add(this.mtxtTenNhanVien);
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1,
            this.ribbonPage3,
            this.ribbonPage2});
            this.ribbonControl1.ShowExpandCollapseButton = DevExpress.Utils.DefaultBoolean.True;
            this.ribbonControl1.ShowFullScreenButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonControl1.ShowToolbarCustomizeItem = false;
            this.ribbonControl1.Size = new System.Drawing.Size(1011, 144);
            this.ribbonControl1.Toolbar.ShowCustomizeItem = false;
            // 
            // mbtnTiepNhan
            // 
            this.mbtnTiepNhan.Caption = "Tiếp nhận";
            this.mbtnTiepNhan.Enabled = false;
            this.mbtnTiepNhan.Glyph = ((System.Drawing.Image)(resources.GetObject("mbtnTiepNhan.Glyph")));
            this.mbtnTiepNhan.Id = 1;
            this.mbtnTiepNhan.Name = "mbtnTiepNhan";
            this.mbtnTiepNhan.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.mbtnTiepNhan.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mbtnTiepNhan_ItemClick);
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "Thêm nhân viên";
            this.barButtonItem3.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem3.Glyph")));
            this.barButtonItem3.Id = 3;
            this.barButtonItem3.Name = "barButtonItem3";
            this.barButtonItem3.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "Sửa thông tin";
            this.barButtonItem4.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem4.Glyph")));
            this.barButtonItem4.Id = 4;
            this.barButtonItem4.Name = "barButtonItem4";
            this.barButtonItem4.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonItem5
            // 
            this.barButtonItem5.Caption = "Danh sách nhân viên";
            this.barButtonItem5.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem5.Glyph")));
            this.barButtonItem5.Id = 5;
            this.barButtonItem5.Name = "barButtonItem5";
            this.barButtonItem5.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // mbtnThuoc
            // 
            this.mbtnThuoc.Caption = "Quản lý thuốc";
            this.mbtnThuoc.Enabled = false;
            this.mbtnThuoc.Glyph = ((System.Drawing.Image)(resources.GetObject("mbtnThuoc.Glyph")));
            this.mbtnThuoc.Id = 6;
            this.mbtnThuoc.Name = "mbtnThuoc";
            this.mbtnThuoc.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.mbtnThuoc.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mbtnThuoc_ItemClick);
            // 
            // mbtnTtb
            // 
            this.mbtnTtb.Caption = "Quản lý trang thiết bị";
            this.mbtnTtb.Enabled = false;
            this.mbtnTtb.Glyph = ((System.Drawing.Image)(resources.GetObject("mbtnTtb.Glyph")));
            this.mbtnTtb.Id = 7;
            this.mbtnTtb.Name = "mbtnTtb";
            this.mbtnTtb.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.mbtnTtb.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mbtnTtb_ItemClick);
            // 
            // barButtonItem8
            // 
            this.barButtonItem8.Caption = "Đăng nhập";
            this.barButtonItem8.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem8.Glyph")));
            this.barButtonItem8.Id = 9;
            this.barButtonItem8.Name = "barButtonItem8";
            this.barButtonItem8.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // mbtnQuanLyNS
            // 
            this.mbtnQuanLyNS.Caption = "Quản lý nhân sự";
            this.mbtnQuanLyNS.Enabled = false;
            this.mbtnQuanLyNS.Glyph = global::QuanLyBenhVien.Properties.Resources.nhansu;
            this.mbtnQuanLyNS.Id = 10;
            this.mbtnQuanLyNS.Name = "mbtnQuanLyNS";
            this.mbtnQuanLyNS.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.mbtnQuanLyNS.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mbtnQuanLyNS_ItemClick);
            // 
            // mbtnDangNhap
            // 
            this.mbtnDangNhap.Caption = "Đăng nhập";
            this.mbtnDangNhap.Glyph = ((System.Drawing.Image)(resources.GetObject("mbtnDangNhap.Glyph")));
            this.mbtnDangNhap.Id = 12;
            this.mbtnDangNhap.Name = "mbtnDangNhap";
            this.mbtnDangNhap.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.mbtnDangNhap.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mbtnDangNhap_ItemClick);
            // 
            // mbtnDangXuat
            // 
            this.mbtnDangXuat.Caption = "Đăng xuất";
            this.mbtnDangXuat.Enabled = false;
            this.mbtnDangXuat.Glyph = global::QuanLyBenhVien.Properties.Resources.stock_lock;
            this.mbtnDangXuat.Id = 13;
            this.mbtnDangXuat.Name = "mbtnDangXuat";
            this.mbtnDangXuat.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.mbtnDangXuat.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mbtnDangXuat_ItemClick);
            // 
            // mbtnThayDoiMK
            // 
            this.mbtnThayDoiMK.Caption = "Thay đổi MK";
            this.mbtnThayDoiMK.Enabled = false;
            this.mbtnThayDoiMK.Glyph = ((System.Drawing.Image)(resources.GetObject("mbtnThayDoiMK.Glyph")));
            this.mbtnThayDoiMK.Id = 14;
            this.mbtnThayDoiMK.Name = "mbtnThayDoiMK";
            this.mbtnThayDoiMK.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.mbtnThayDoiMK.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mbtnThayDoiMK_ItemClick);
            // 
            // mbtnBaoCaoTon
            // 
            this.mbtnBaoCaoTon.Caption = "Báo cáo tồn";
            this.mbtnBaoCaoTon.Enabled = false;
            this.mbtnBaoCaoTon.Glyph = ((System.Drawing.Image)(resources.GetObject("mbtnBaoCaoTon.Glyph")));
            this.mbtnBaoCaoTon.Id = 15;
            this.mbtnBaoCaoTon.Name = "mbtnBaoCaoTon";
            this.mbtnBaoCaoTon.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // mbtnCauHinh
            // 
            this.mbtnCauHinh.Caption = "Cấu hình";
            this.mbtnCauHinh.Enabled = false;
            this.mbtnCauHinh.Glyph = global::QuanLyBenhVien.Properties.Resources.dialog_information;
            this.mbtnCauHinh.Id = 18;
            this.mbtnCauHinh.Name = "mbtnCauHinh";
            this.mbtnCauHinh.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.mbtnCauHinh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mbtnCauHinh_ItemClick);
            // 
            // mtxtTenNhanVien
            // 
            this.mtxtTenNhanVien.Caption = "Khách";
            this.mtxtTenNhanVien.Id = 19;
            this.mtxtTenNhanVien.Name = "mtxtTenNhanVien";
            this.mtxtTenNhanVien.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // ribbonGalleryBarItem1
            // 
            this.ribbonGalleryBarItem1.Caption = "ribbonGalleryBarItem1";
            this.ribbonGalleryBarItem1.Id = 20;
            this.ribbonGalleryBarItem1.Name = "ribbonGalleryBarItem1";
            // 
            // mbtnKhamBenh
            // 
            this.mbtnKhamBenh.Caption = "Khám bệnh";
            this.mbtnKhamBenh.Enabled = false;
            this.mbtnKhamBenh.Glyph = global::QuanLyBenhVien.Properties.Resources.khambenh;
            this.mbtnKhamBenh.Id = 21;
            this.mbtnKhamBenh.Name = "mbtnKhamBenh";
            this.mbtnKhamBenh.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.mbtnKhamBenh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnKhamBenh_ItemClick);
            // 
            // mbtnThuPhi
            // 
            this.mbtnThuPhi.Caption = "Thu phí";
            this.mbtnThuPhi.Enabled = false;
            this.mbtnThuPhi.Glyph = global::QuanLyBenhVien.Properties.Resources.vienphi;
            this.mbtnThuPhi.Id = 25;
            this.mbtnThuPhi.Name = "mbtnThuPhi";
            this.mbtnThuPhi.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.mbtnThuPhi.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mbtnThuPhi_ItemClick);
            // 
            // mbtnQuanLyBL
            // 
            this.mbtnQuanLyBL.Caption = "Quản lý bệnh lý";
            this.mbtnQuanLyBL.Enabled = false;
            this.mbtnQuanLyBL.Glyph = global::QuanLyBenhVien.Properties.Resources.id_p;
            this.mbtnQuanLyBL.Id = 26;
            this.mbtnQuanLyBL.Name = "mbtnQuanLyBL";
            this.mbtnQuanLyBL.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.mbtnQuanLyBL.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mbtnQuanLyBL_ItemClick);
            // 
            // mbtnQuanLyVP
            // 
            this.mbtnQuanLyVP.Caption = "Quản lý viện phí";
            this.mbtnQuanLyVP.Enabled = false;
            this.mbtnQuanLyVP.Glyph = global::QuanLyBenhVien.Properties.Resources.vienphi;
            this.mbtnQuanLyVP.Id = 27;
            this.mbtnQuanLyVP.Name = "mbtnQuanLyVP";
            this.mbtnQuanLyVP.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.mbtnQuanLyVP.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mbtnQuanLyVP_ItemClick);
            // 
            // mbtnQuanLyCLS
            // 
            this.mbtnQuanLyCLS.Caption = "Quản lý cận lâm sàng";
            this.mbtnQuanLyCLS.Enabled = false;
            this.mbtnQuanLyCLS.Glyph = global::QuanLyBenhVien.Properties.Resources.canlamsan;
            this.mbtnQuanLyCLS.Id = 28;
            this.mbtnQuanLyCLS.Name = "mbtnQuanLyCLS";
            this.mbtnQuanLyCLS.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.mbtnQuanLyCLS.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mbtnQuanLyCLS_ItemClick);
            // 
            // mbtnBaoCaoDoanhThu
            // 
            this.mbtnBaoCaoDoanhThu.Caption = "Báo cáo doanh thu theo tháng";
            this.mbtnBaoCaoDoanhThu.Enabled = false;
            this.mbtnBaoCaoDoanhThu.Glyph = ((System.Drawing.Image)(resources.GetObject("mbtnBaoCaoDoanhThu.Glyph")));
            this.mbtnBaoCaoDoanhThu.Id = 29;
            this.mbtnBaoCaoDoanhThu.Name = "mbtnBaoCaoDoanhThu";
            this.mbtnBaoCaoDoanhThu.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1,
            this.ribbonPageGroup7,
            this.ribbonPageGroup8,
            this.ribbonPageGroup5,
            this.ribbonPageGroup6});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "Trang chủ";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.mbtnTiepNhan);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.ShowCaptionButton = false;
            this.ribbonPageGroup1.Text = "Bệnh nhân";
            // 
            // ribbonPageGroup7
            // 
            this.ribbonPageGroup7.AllowTextClipping = false;
            this.ribbonPageGroup7.ItemLinks.Add(this.mbtnKhamBenh);
            this.ribbonPageGroup7.Name = "ribbonPageGroup7";
            this.ribbonPageGroup7.ShowCaptionButton = false;
            this.ribbonPageGroup7.Text = "Khám bệnh";
            // 
            // ribbonPageGroup8
            // 
            this.ribbonPageGroup8.AllowTextClipping = false;
            this.ribbonPageGroup8.ItemLinks.Add(this.mbtnThuPhi);
            this.ribbonPageGroup8.Name = "ribbonPageGroup8";
            this.ribbonPageGroup8.ShowCaptionButton = false;
            this.ribbonPageGroup8.Text = "Viện phí";
            // 
            // ribbonPageGroup5
            // 
            this.ribbonPageGroup5.ItemLinks.Add(this.mbtnBaoCaoTon);
            this.ribbonPageGroup5.ItemLinks.Add(this.mbtnBaoCaoDoanhThu);
            this.ribbonPageGroup5.Name = "ribbonPageGroup5";
            this.ribbonPageGroup5.ShowCaptionButton = false;
            this.ribbonPageGroup5.Text = "Báo cáo";
            // 
            // ribbonPageGroup6
            // 
            this.ribbonPageGroup6.ItemLinks.Add(this.mbtnDangNhap);
            this.ribbonPageGroup6.ItemLinks.Add(this.mbtnDangXuat);
            this.ribbonPageGroup6.ItemLinks.Add(this.mbtnThayDoiMK);
            this.ribbonPageGroup6.ItemLinks.Add(this.mbtnCauHinh);
            this.ribbonPageGroup6.Name = "ribbonPageGroup6";
            this.ribbonPageGroup6.ShowCaptionButton = false;
            this.ribbonPageGroup6.Text = "Hệ thống";
            // 
            // ribbonPage3
            // 
            this.ribbonPage3.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup2,
            this.ribbonPageGroup4,
            this.ribbonPageGroup9,
            this.ribbonPageGroup10,
            this.ribbonPageGroup11});
            this.ribbonPage3.Name = "ribbonPage3";
            this.ribbonPage3.Text = "Quản trị";
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ItemLinks.Add(this.mbtnQuanLyNS);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.ShowCaptionButton = false;
            this.ribbonPageGroup2.Text = "Nhân viên";
            // 
            // ribbonPageGroup4
            // 
            this.ribbonPageGroup4.ItemLinks.Add(this.mbtnThuoc);
            this.ribbonPageGroup4.ItemLinks.Add(this.mbtnTtb);
            this.ribbonPageGroup4.Name = "ribbonPageGroup4";
            this.ribbonPageGroup4.ShowCaptionButton = false;
            this.ribbonPageGroup4.Text = "Cơ sở vật chất";
            // 
            // ribbonPageGroup9
            // 
            this.ribbonPageGroup9.AllowTextClipping = false;
            this.ribbonPageGroup9.ItemLinks.Add(this.mbtnQuanLyBL);
            this.ribbonPageGroup9.Name = "ribbonPageGroup9";
            this.ribbonPageGroup9.ShowCaptionButton = false;
            this.ribbonPageGroup9.Text = "Bệnh lý";
            // 
            // ribbonPageGroup10
            // 
            this.ribbonPageGroup10.AllowTextClipping = false;
            this.ribbonPageGroup10.ItemLinks.Add(this.mbtnQuanLyVP);
            this.ribbonPageGroup10.Name = "ribbonPageGroup10";
            this.ribbonPageGroup10.ShowCaptionButton = false;
            this.ribbonPageGroup10.Text = "Viện phí";
            // 
            // ribbonPageGroup11
            // 
            this.ribbonPageGroup11.AllowTextClipping = false;
            this.ribbonPageGroup11.ItemLinks.Add(this.mbtnQuanLyCLS);
            this.ribbonPageGroup11.Name = "ribbonPageGroup11";
            this.ribbonPageGroup11.ShowCaptionButton = false;
            this.ribbonPageGroup11.Text = "Cận lâm sàng";
            // 
            // ribbonPage2
            // 
            this.ribbonPage2.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup3});
            this.ribbonPage2.Name = "ribbonPage2";
            this.ribbonPage2.Text = "Giao diện";
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.AllowTextClipping = false;
            this.ribbonPageGroup3.ItemLinks.Add(this.ribbonGalleryBarItem1);
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.ShowCaptionButton = false;
            this.ribbonPageGroup3.Text = "Thay đổi giao diện";
            // 
            // mxtraTabControl
            // 
            this.mxtraTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mxtraTabControl.HeaderButtons = DevExpress.XtraTab.TabButtons.None;
            this.mxtraTabControl.HeaderButtonsShowMode = DevExpress.XtraTab.TabButtonShowMode.Never;
            this.mxtraTabControl.Location = new System.Drawing.Point(0, 144);
            this.mxtraTabControl.Name = "mxtraTabControl";
            this.mxtraTabControl.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;
            this.mxtraTabControl.Size = new System.Drawing.Size(1011, 487);
            this.mxtraTabControl.TabIndex = 3;
            // 
            // frmMainpage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1011, 631);
            this.Controls.Add(this.mxtraTabControl);
            this.Controls.Add(this.ribbonControl1);
            this.Name = "frmMainpage";
            this.Ribbon = this.ribbonControl1;
            this.Text = "Phần mềm quản lý bệnh viện";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmMainpage_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mxtraTabControl)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraBars.BarButtonItem barButtonItem5;
        private DevExpress.XtraBars.BarButtonItem mbtnThuoc;
        private DevExpress.XtraBars.BarButtonItem mbtnTtb;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup4;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup5;
        private DevExpress.XtraBars.BarButtonItem barButtonItem8;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage3;
        private DevExpress.XtraBars.BarButtonItem mbtnQuanLyNS;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        public DevExpress.XtraTab.XtraTabControl mxtraTabControl;
        private DevExpress.XtraBars.BarButtonItem mbtnDangNhap;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup6;
        private DevExpress.XtraBars.BarButtonItem mbtnDangXuat;
        private DevExpress.XtraBars.BarButtonItem mbtnThayDoiMK;
        private DevExpress.XtraBars.BarButtonItem mbtnCauHinh;
        private DevExpress.XtraBars.BarStaticItem mtxtTenNhanVien;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage2;
        private DevExpress.XtraBars.RibbonGalleryBarItem ribbonGalleryBarItem1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup7;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup8;
        private DevExpress.XtraBars.BarButtonItem mbtnQuanLyBL;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup9;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup10;
        private DevExpress.XtraBars.BarButtonItem mbtnQuanLyVP;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup11;
        private DevExpress.XtraBars.BarButtonItem mbtnQuanLyCLS;
        public DevExpress.XtraBars.BarButtonItem mbtnTiepNhan;
        public DevExpress.XtraBars.BarButtonItem mbtnBaoCaoTon;
        public DevExpress.XtraBars.BarButtonItem mbtnKhamBenh;
        public DevExpress.XtraBars.BarButtonItem mbtnThuPhi;
        public DevExpress.XtraBars.BarButtonItem mbtnBaoCaoDoanhThu;
    }
}

