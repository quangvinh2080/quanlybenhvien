﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using BLL;
using DevExpress.XtraGrid.Views.Base;

namespace QuanLyBenhVien
{
    public partial class frmQuanLyThuoc : DevExpress.XtraEditors.XtraForm
    {
        BangThuoc bt = new BangThuoc();
        public static frmQuanLyThuoc pqltInstance;
        public frmQuanLyThuoc()
        {
            InitializeComponent();
            pqltInstance = this;
        }

        private void frmQuanLyThuoc_Load(object sender, EventArgs e)
        {
            gridControl1.DataSource = BangThuoc.btLayThongTin();
            mgvThuoc.Columns[0].FieldName = "MaThuoc";
            mgvThuoc.Columns[1].FieldName = "TenThuoc";
            mgvThuoc.Columns[2].FieldName = "SoLuong";
            mgvThuoc.Columns[3].FieldName = "DonGia";
            mgvThuoc.Columns[4].FieldName = "MaDVT";
            mgvThuoc.Columns[5].FieldName = "MaDuongDung";
            mgvThuoc.Columns[6].FieldName = "MaNhomThuoc";

            repositoryItemLookUpEdit1.DataSource = BangDVT.bdvtLayThongTin();
            repositoryItemLookUpEdit1.DisplayMember = "TenDVT";
            repositoryItemLookUpEdit1.ValueMember = "MaDVT";

            repositoryItemLookUpEdit2.DataSource = BangDuongDung.bddLayThongTin();
            repositoryItemLookUpEdit2.DisplayMember = "TenDuongDung";
            repositoryItemLookUpEdit2.ValueMember = "MaDuongDung";

            repositoryItemLookUpEdit3.DataSource = BangNhomThuoc.bntLayThongTin();
            repositoryItemLookUpEdit3.DisplayMember = "TenNhomThuoc";
            repositoryItemLookUpEdit3.ValueMember = "MaNhomThuoc";
        }

        private void mgvThuoc_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            string maThuoc = "";
            if (mgvThuoc.RowCount == 0)
                maThuoc = "T0001";
            else 
            {
                maThuoc = HamTienIch.htiChuyenDoiMa1KyTu(BangThuoc.btLayMaThuocCuoi(), "T");
            } 
            ColumnView View = sender as ColumnView;
            View.SetRowCellValue(e.RowHandle, View.Columns["MaThuoc"], maThuoc);
        }

        private void mgvThuoc_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            if (e.RowHandle < 0)
            {
                bt.btThemThuoc(mgvThuoc.GetFocusedRowCellValue("MaThuoc").ToString(), mgvThuoc.GetFocusedRowCellValue("TenThuoc").ToString(), (int)mgvThuoc.GetFocusedRowCellValue("SoLuong"), (int)mgvThuoc.GetFocusedRowCellValue("DonGia"), mgvThuoc.GetFocusedRowCellValue("MaDVT").ToString(), mgvThuoc.GetFocusedRowCellValue("MaDuongDung").ToString(), mgvThuoc.GetFocusedRowCellValue("MaNhomThuoc").ToString());
                XtraMessageBox.Show("Thêm thuốc thành công");
            }
            else
            {
                bt.btSuaThuoc(mgvThuoc.GetFocusedRowCellValue("MaThuoc").ToString(), mgvThuoc.GetFocusedRowCellValue("TenThuoc").ToString(), (int)mgvThuoc.GetFocusedRowCellValue("SoLuong"), (int)mgvThuoc.GetFocusedRowCellValue("DonGia"), mgvThuoc.GetFocusedRowCellValue("MaDVT").ToString(), mgvThuoc.GetFocusedRowCellValue("MaDuongDung").ToString(), mgvThuoc.GetFocusedRowCellValue("MaNhomThuoc").ToString());
                XtraMessageBox.Show("Cập nhật thành công");
            }
        }

        private void btnThemNhom_Click(object sender, EventArgs e)
        {
            frmThemNhomThuoc frm_ThemNhomThuoc = new frmThemNhomThuoc();
            frm_ThemNhomThuoc.ShowDialog();
        }

        private void txtTimKiem_TextChanged(object sender, EventArgs e)
        {
            if (txtTimKiem.Text != "")
            {
                switch (rdgTimTheo.Text)
                {
                    case "name":
                        mgvThuoc.ApplyFindFilter("Tên:\"" + txtTimKiem.Text + "\"");
                        break;
                    case "dvt":
                        mgvThuoc.ApplyFindFilter("\"Đơn vị tính\":\"" + txtTimKiem.Text + "\"");
                        break;
                    case "duongdung":
                        mgvThuoc.ApplyFindFilter("Đường:\"" + txtTimKiem.Text + "\"");
                        break;
                    case "group":
                        mgvThuoc.ApplyFindFilter("Nhóm:\"" + txtTimKiem.Text + "\"");
                        break;
                }
            }
            else
            {
                mgvThuoc.ApplyFindFilter("");
            }
        }
    }
}