﻿namespace QuanLyBenhVien
{
    partial class frmQuanLyNhanSu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmQuanLyNhanSu));
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.btnLamTuoi = new DevExpress.XtraEditors.SimpleButton();
            this.rdgTimTheo = new DevExpress.XtraEditors.RadioGroup();
            this.mbtnSua = new DevExpress.XtraEditors.SimpleButton();
            this.mbtnThemMoi = new DevExpress.XtraEditors.SimpleButton();
            this.mbtnXoa = new DevExpress.XtraEditors.SimpleButton();
            this.txtTraCuu = new DevExpress.XtraEditors.TextEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.mgvNhanSu = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAnh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaNV = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTenNV = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTenDN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNgaySinh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGioiTinh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDiaChi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDienThoai = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHocHam = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHocVi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNghiepVu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKhoa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rdgTimTheo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTraCuu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mgvNhanSu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.btnLamTuoi);
            this.layoutControl1.Controls.Add(this.rdgTimTheo);
            this.layoutControl1.Controls.Add(this.mbtnSua);
            this.layoutControl1.Controls.Add(this.mbtnThemMoi);
            this.layoutControl1.Controls.Add(this.mbtnXoa);
            this.layoutControl1.Controls.Add(this.txtTraCuu);
            this.layoutControl1.Controls.Add(this.gridControl1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(66, 107, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1300, 500);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // btnLamTuoi
            // 
            this.btnLamTuoi.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnLamTuoi.Appearance.Options.UseFont = true;
            this.btnLamTuoi.Image = global::QuanLyBenhVien.Properties.Resources.refresh;
            this.btnLamTuoi.Location = new System.Drawing.Point(12, 458);
            this.btnLamTuoi.Name = "btnLamTuoi";
            this.btnLamTuoi.Size = new System.Drawing.Size(120, 30);
            this.btnLamTuoi.StyleController = this.layoutControl1;
            this.btnLamTuoi.TabIndex = 11;
            this.btnLamTuoi.Text = "Làm tươi";
            this.btnLamTuoi.Click += new System.EventHandler(this.btnLamTuoi_Click);
            // 
            // rdgTimTheo
            // 
            this.rdgTimTheo.EditValue = "name";
            this.rdgTimTheo.Location = new System.Drawing.Point(79, 43);
            this.rdgTimTheo.Name = "rdgTimTheo";
            this.rdgTimTheo.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("name", "Tên"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("year", "Năm sinh"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("type", "Nghiệp vụ")});
            this.rdgTimTheo.Size = new System.Drawing.Size(232, 25);
            this.rdgTimTheo.StyleController = this.layoutControl1;
            this.rdgTimTheo.TabIndex = 10;
            // 
            // mbtnSua
            // 
            this.mbtnSua.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.mbtnSua.Appearance.Options.UseFont = true;
            this.mbtnSua.Image = global::QuanLyBenhVien.Properties.Resources.modify24x24;
            this.mbtnSua.Location = new System.Drawing.Point(1012, 458);
            this.mbtnSua.Name = "mbtnSua";
            this.mbtnSua.Size = new System.Drawing.Size(136, 30);
            this.mbtnSua.StyleController = this.layoutControl1;
            this.mbtnSua.TabIndex = 9;
            this.mbtnSua.Text = "Sửa >>>";
            this.mbtnSua.Click += new System.EventHandler(this.mbtnSua_Click);
            // 
            // mbtnThemMoi
            // 
            this.mbtnThemMoi.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.mbtnThemMoi.Appearance.Options.UseFont = true;
            this.mbtnThemMoi.Image = ((System.Drawing.Image)(resources.GetObject("mbtnThemMoi.Image")));
            this.mbtnThemMoi.Location = new System.Drawing.Point(877, 458);
            this.mbtnThemMoi.Name = "mbtnThemMoi";
            this.mbtnThemMoi.Size = new System.Drawing.Size(131, 30);
            this.mbtnThemMoi.StyleController = this.layoutControl1;
            this.mbtnThemMoi.TabIndex = 8;
            this.mbtnThemMoi.Text = "Thêm mới>>>";
            this.mbtnThemMoi.Click += new System.EventHandler(this.mbtnThemMoi_Click);
            // 
            // mbtnXoa
            // 
            this.mbtnXoa.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.mbtnXoa.Appearance.Options.UseFont = true;
            this.mbtnXoa.Image = global::QuanLyBenhVien.Properties.Resources.cancel;
            this.mbtnXoa.Location = new System.Drawing.Point(1152, 458);
            this.mbtnXoa.Name = "mbtnXoa";
            this.mbtnXoa.Size = new System.Drawing.Size(136, 30);
            this.mbtnXoa.StyleController = this.layoutControl1;
            this.mbtnXoa.TabIndex = 7;
            this.mbtnXoa.Text = "Xoá";
            this.mbtnXoa.Click += new System.EventHandler(this.mbtnXoa_Click);
            // 
            // txtTraCuu
            // 
            this.txtTraCuu.Location = new System.Drawing.Point(80, 17);
            this.txtTraCuu.Name = "txtTraCuu";
            this.txtTraCuu.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtTraCuu.Properties.Appearance.Options.UseFont = true;
            this.txtTraCuu.Size = new System.Drawing.Size(304, 22);
            this.txtTraCuu.StyleController = this.layoutControl1;
            this.txtTraCuu.TabIndex = 5;
            this.txtTraCuu.TextChanged += new System.EventHandler(this.txtTraCuu_TextChanged);
            // 
            // gridControl1
            // 
            this.gridControl1.Location = new System.Drawing.Point(12, 72);
            this.gridControl1.MainView = this.mgvNhanSu;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(1276, 382);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.mgvNhanSu});
            // 
            // mgvNhanSu
            // 
            this.mgvNhanSu.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAnh,
            this.colMaNV,
            this.colTenNV,
            this.colTenDN,
            this.colNgaySinh,
            this.colGioiTinh,
            this.colDiaChi,
            this.colDienThoai,
            this.colHocHam,
            this.colHocVi,
            this.colNghiepVu,
            this.colKhoa});
            this.mgvNhanSu.CustomizationFormBounds = new System.Drawing.Rectangle(1137, 449, 210, 172);
            this.mgvNhanSu.GridControl = this.gridControl1;
            this.mgvNhanSu.Name = "mgvNhanSu";
            this.mgvNhanSu.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.mgvNhanSu.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.True;
            this.mgvNhanSu.OptionsBehavior.Editable = false;
            this.mgvNhanSu.OptionsBehavior.ReadOnly = true;
            this.mgvNhanSu.OptionsFind.AllowFindPanel = false;
            this.mgvNhanSu.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.mgvNhanSu.OptionsView.ShowGroupPanel = false;
            // 
            // colAnh
            // 
            this.colAnh.Caption = "Ảnh";
            this.colAnh.Name = "colAnh";
            this.colAnh.Visible = true;
            this.colAnh.VisibleIndex = 0;
            this.colAnh.Width = 70;
            // 
            // colMaNV
            // 
            this.colMaNV.Caption = "Mã nhân viên";
            this.colMaNV.Name = "colMaNV";
            this.colMaNV.Visible = true;
            this.colMaNV.VisibleIndex = 1;
            this.colMaNV.Width = 117;
            // 
            // colTenNV
            // 
            this.colTenNV.Caption = "Họ tên";
            this.colTenNV.Name = "colTenNV";
            this.colTenNV.Visible = true;
            this.colTenNV.VisibleIndex = 2;
            this.colTenNV.Width = 117;
            // 
            // colTenDN
            // 
            this.colTenDN.Caption = "Tên đăng nhập";
            this.colTenDN.Name = "colTenDN";
            this.colTenDN.Visible = true;
            this.colTenDN.VisibleIndex = 3;
            this.colTenDN.Width = 117;
            // 
            // colNgaySinh
            // 
            this.colNgaySinh.Caption = "Ngày sinh";
            this.colNgaySinh.Name = "colNgaySinh";
            this.colNgaySinh.Visible = true;
            this.colNgaySinh.VisibleIndex = 4;
            this.colNgaySinh.Width = 90;
            // 
            // colGioiTinh
            // 
            this.colGioiTinh.Caption = "Giới tính";
            this.colGioiTinh.Name = "colGioiTinh";
            this.colGioiTinh.Visible = true;
            this.colGioiTinh.VisibleIndex = 5;
            this.colGioiTinh.Width = 49;
            // 
            // colDiaChi
            // 
            this.colDiaChi.Caption = "Địa chỉ";
            this.colDiaChi.Name = "colDiaChi";
            this.colDiaChi.Visible = true;
            this.colDiaChi.VisibleIndex = 6;
            this.colDiaChi.Width = 137;
            // 
            // colDienThoai
            // 
            this.colDienThoai.Caption = "Điện thoại";
            this.colDienThoai.Name = "colDienThoai";
            this.colDienThoai.Visible = true;
            this.colDienThoai.VisibleIndex = 7;
            this.colDienThoai.Width = 137;
            // 
            // colHocHam
            // 
            this.colHocHam.Caption = "Học hàm";
            this.colHocHam.Name = "colHocHam";
            this.colHocHam.Visible = true;
            this.colHocHam.VisibleIndex = 8;
            this.colHocHam.Width = 114;
            // 
            // colHocVi
            // 
            this.colHocVi.Caption = "Học vị";
            this.colHocVi.Name = "colHocVi";
            this.colHocVi.Visible = true;
            this.colHocVi.VisibleIndex = 9;
            this.colHocVi.Width = 100;
            // 
            // colNghiepVu
            // 
            this.colNghiepVu.Caption = "Nghiệp vụ";
            this.colNghiepVu.Name = "colNghiepVu";
            this.colNghiepVu.Visible = true;
            this.colNghiepVu.VisibleIndex = 10;
            this.colNghiepVu.Width = 115;
            // 
            // colKhoa
            // 
            this.colKhoa.Caption = "Khoa";
            this.colKhoa.Name = "colKhoa";
            this.colKhoa.Visible = true;
            this.colKhoa.VisibleIndex = 11;
            this.colKhoa.Width = 95;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.layoutControlGroup1.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup1.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.layoutControlGroup1.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.emptySpaceItem3,
            this.emptySpaceItem1,
            this.layoutControlItem3,
            this.emptySpaceItem2,
            this.layoutControlItem7});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1300, 500);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gridControl1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 60);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1280, 386);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem2.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.layoutControlItem2.Control = this.txtTraCuu;
            this.layoutControlItem2.CustomizationFormText = "Tìm theo tên bệnh nhân :";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 7, 2);
            this.layoutControlItem2.Size = new System.Drawing.Size(376, 31);
            this.layoutControlItem2.Text = "Tìm kiếm :";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(63, 16);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.mbtnXoa;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(1140, 446);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(140, 34);
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.mbtnThemMoi;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(865, 446);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(135, 34);
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.mbtnSua;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(1000, 446);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(140, 34);
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(124, 446);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(741, 34);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(376, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(904, 31);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.rdgTimTheo;
            this.layoutControlItem3.CustomizationFormText = "Tìm theo :";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 31);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(303, 29);
            this.layoutControlItem3.Text = "Tìm theo :";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(64, 16);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(303, 31);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(977, 29);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.btnLamTuoi;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 446);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(124, 34);
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // frmQuanLyNhanSu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1300, 500);
            this.Controls.Add(this.layoutControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmQuanLyNhanSu";
            this.Text = "frmQuanLyNhanSu";
            this.Load += new System.EventHandler(this.frmQuanLyNhanSu_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rdgTimTheo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTraCuu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mgvNhanSu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraGrid.Views.Grid.GridView mgvNhanSu;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.TextEdit txtTraCuu;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.SimpleButton mbtnSua;
        private DevExpress.XtraEditors.SimpleButton mbtnThemMoi;
        private DevExpress.XtraEditors.SimpleButton mbtnXoa;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraEditors.RadioGroup rdgTimTheo;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        public DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraEditors.SimpleButton btnLamTuoi;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraGrid.Columns.GridColumn colMaNV;
        private DevExpress.XtraGrid.Columns.GridColumn colTenNV;
        private DevExpress.XtraGrid.Columns.GridColumn colTenDN;
        private DevExpress.XtraGrid.Columns.GridColumn colNgaySinh;
        private DevExpress.XtraGrid.Columns.GridColumn colGioiTinh;
        private DevExpress.XtraGrid.Columns.GridColumn colDiaChi;
        private DevExpress.XtraGrid.Columns.GridColumn colDienThoai;
        private DevExpress.XtraGrid.Columns.GridColumn colHocHam;
        private DevExpress.XtraGrid.Columns.GridColumn colHocVi;
        private DevExpress.XtraGrid.Columns.GridColumn colNghiepVu;
        private DevExpress.XtraGrid.Columns.GridColumn colKhoa;
        private DevExpress.XtraGrid.Columns.GridColumn colAnh;
    }
}