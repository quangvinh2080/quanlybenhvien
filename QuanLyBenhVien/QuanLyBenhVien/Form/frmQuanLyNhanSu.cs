﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraSplashScreen;
using BLL;

namespace QuanLyBenhVien
{
    public partial class frmQuanLyNhanSu : DevExpress.XtraEditors.XtraForm
    {
        BangNhanVien bnv = new BangNhanVien();
        public static frmQuanLyNhanSu pqlnsInstance;
        public frmQuanLyNhanSu()
        {
            InitializeComponent();
            pqlnsInstance = this;
        }

        private void mbtnThemMoi_Click(object sender, EventArgs e)
        {
            SplashScreenManager.ShowForm(typeof(WaitForm1));
            frmThemNV frm_ThemNV = new frmThemNV();
            DieuKhien.dkTaoTab(QuanLyBenhVien.frmMainpage.pfrmInstance.frm_Tabmain.xtraTabControl1, "Thêm nhân viên", frm_ThemNV.Name, frm_ThemNV);
            SplashScreenManager.CloseForm();
        }

        private void mbtnSua_Click(object sender, EventArgs e)
        {
            //lấy thông tin nhân viên được select
            int[] rowSelected = mgvNhanSu.GetSelectedRows();
            string maNV = "";
            if (rowSelected[0] >= 0)
                maNV = mgvNhanSu.GetRowCellValue(rowSelected[0], "MaNV").ToString();
            else
                XtraMessageBox.Show("Bạn chưa chọn nhân viên");
            
            SplashScreenManager.ShowForm(typeof(WaitForm1));
            frmSuaNV frm_SuaNV = new frmSuaNV(maNV);
            DieuKhien.dkTaoTab(QuanLyBenhVien.frmMainpage.pfrmInstance.frm_Tabmain.xtraTabControl1, "Sửa thông tin nhân viên", frm_SuaNV.Name, frm_SuaNV);
            SplashScreenManager.CloseForm();
        }

        private void frmQuanLyNhanSu_Load(object sender, EventArgs e)
        {
            //bound data source
            gridControl1.DataSource = BangNhanVien.bnvLayThongTinNV();
            mgvNhanSu.Columns[0].FieldName = "Anh";
            mgvNhanSu.Columns[1].FieldName = "MaNV";
            mgvNhanSu.Columns[2].FieldName = "HoTenNV";
            mgvNhanSu.Columns[3].FieldName = "TenDangNhap";
            mgvNhanSu.Columns[4].FieldName = "NgaySinh";
            mgvNhanSu.Columns[5].FieldName = "GioiTinh";
            mgvNhanSu.Columns[6].FieldName = "DiaChi";
            mgvNhanSu.Columns[7].FieldName = "DienThoai";
            mgvNhanSu.Columns[8].FieldName = "TenHocHam";
            mgvNhanSu.Columns[9].FieldName = "TenHocVi";
            mgvNhanSu.Columns[10].FieldName = "TenNghiepVu";
            mgvNhanSu.Columns[11].FieldName = "TenKhoa";

        }

        private void txtTraCuu_TextChanged(object sender, EventArgs e)
        {
            if(txtTraCuu.Text == "")
            {   mgvNhanSu.ApplyFindFilter("");
            
            }
            else{
                switch (rdgTimTheo.Text) 
                {
                    case "name"://tim theo ten
                        mgvNhanSu.ApplyFindFilter("Họ:\""+txtTraCuu.Text+"\"");
                        break;
                    case "year"://tim theo nam sinh
                        mgvNhanSu.ApplyFindFilter("Ngày:\"" + txtTraCuu.Text + "\"");
                        break;
                    case "type"://tim theo chuc vu
                        mgvNhanSu.ApplyFindFilter("Nghiệp:\"" + txtTraCuu.Text + "\"");
                        break;
            
                }
            }
        }

        private void btnLamTuoi_Click(object sender, EventArgs e)
        {
            gridControl1.DataSource = BangNhanVien.bnvLayThongTinNV();
        }

        private void mbtnXoa_Click(object sender, EventArgs e)
        {
            if (XtraMessageBox.Show("Bạn muốn xoá nhân viên này?", "Xoá nhân viên", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                //lấy thông tin được select
                int[] rowSelected = mgvNhanSu.GetSelectedRows();
                string maNV = "";
                if (rowSelected[0] >= 0)
                {
                    maNV = mgvNhanSu.GetRowCellValue(rowSelected[0], "MaNV").ToString();
                    if (bnv.bnvXoaNhanVien(maNV))
                    {
                        XtraMessageBox.Show(this, "Xoá nhân viên thành công", "Xoá nhân viên", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        btnLamTuoi.PerformClick();
                    }
                    else
                    {
                        XtraMessageBox.Show("Có lỗi xảy ra !!!");
                    }

                }
                else
                    XtraMessageBox.Show("Bạn chưa chọn nhân viên");
            }
        }
    }
}