﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using BLL;
using DevExpress.XtraSplashScreen;

namespace QuanLyBenhVien
{
    public partial class frmWelcome : DevExpress.XtraEditors.XtraForm
    {
        public frmWelcome()
        {
            InitializeComponent();
        }
        private void frmWelcome_Load(object sender, EventArgs e)
        {
            loadThongTinBenhVien();
        }
        /// <summary>
        /// load thông tin bệnh viện và nhân viên vào control
        /// </summary>
        private void loadThongTinBenhVien()
        {
            lblDiaChi.Text = BangThamSo.btsDiaChi();
            lblTenBV.Text = BangThamSo.btsTenBenhVien();
            lblTenNV.Text = BienToanCuc.tenNhanVien;
            lblNghiepVu.Text = BangNghiepVu.bnvLayTenNghiepVu(BienToanCuc.maNghiepVu);
            lblSdt.Text = BangThamSo.btsSoDT();
            lblSodd.Text = BangThamSo.btsSoDD();
        }
        private void btnTiepNhan_Click(object sender, EventArgs e)
        {
            QuanLyBenhVien.frmMainpage.pfrmInstance.mbtnTiepNhan.PerformClick();
        }

        private void btnKhamBenh_Click(object sender, EventArgs e)
        {
            QuanLyBenhVien.frmMainpage.pfrmInstance.mbtnKhamBenh.PerformClick();
        }

        private void btnThuPhi_Click(object sender, EventArgs e)
        {
            QuanLyBenhVien.frmMainpage.pfrmInstance.mbtnThuPhi.PerformClick();
        }

        private void btnLapBaoCao_Click(object sender, EventArgs e)
        {
            
        }

    }
}