﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.IO;
using BLL;

namespace QuanLyBenhVien
{
    public partial class frmThemNV : DevExpress.XtraEditors.XtraForm
    {
        BangNhanVien bnv = new BangNhanVien();
        public frmThemNV()
        {
            InitializeComponent();
        }

        private void btnThemHinh_Click(object sender, EventArgs e)
        {
            OpenFileDialog mOfd = new OpenFileDialog();
            mOfd.Filter = "Image Files()|*.jpg|Image Bimap()|*.gif|Image png()|*.png";
            try
            {
                if (mOfd.ShowDialog() == DialogResult.OK)
                {
                    if (File.Exists(mOfd.FileName))
                    {
                        pteAnhNV.Image = Image.FromFile(mOfd.FileName);//ChinhAnh.pfncResizeAnh(mOfd.FileName, mptbAnhNV.Height, mptbAnhNV.Width);
                    }
                }
            }
            catch { }
        }

        private void btnXoaHinh_Click(object sender, EventArgs e)
        {
            pteAnhNV.Image = Properties.Resources.unknown;
        }

        private void frmThemNV_Load(object sender, EventArgs e)
        {
            //Tự động sinh ra mã nhân viên
            string maCuoi = BangNhanVien.bnvLayMaNVCuoi();
            string maMoi = "";
            if (maCuoi == null)
                maMoi = "NV001";
            else 
            {
                maMoi = HamTienIch.htiChuyenDoiMa2KyTu(maCuoi, "NV");
            }
            txtMaNV.Text = maMoi;
            //

            cbbHocVi.DataSource = BangHocVi.bhvLayDsHocVi();
            cbbHocVi.DisplayMember = "TenHocVi";
            cbbHocVi.ValueMember = "MaHocVi";

            cbbHocHam.DataSource = BangHocHam.bhhLayDsHocHam();
            cbbHocHam.DisplayMember = "TenHocHam";
            cbbHocHam.ValueMember = "MaHocHam";

            cbbNghiepVu.DataSource = BangNghiepVu.bnvLayDsNghiepVu();
            cbbNghiepVu.DisplayMember = "TenNghiepVu";
            cbbNghiepVu.ValueMember = "MaNghiepVu";

            cbbKhoa.DataSource = BangKhoa.bkLayThongTin();
            cbbKhoa.DisplayMember = "TenKhoa";
            cbbKhoa.ValueMember = "MaKhoa";

            
        }

        private void txtEdit_Validating(object sender, CancelEventArgs e)
        {
            TextEdit view = sender as TextEdit;
            if (view.Text == "")
            { 
                e.Cancel = true;
                view.ErrorText = "Trường này không được để trống";
            }
            else
            {
                e.Cancel = false;
                view.ErrorText = "";
            }
            
        }

        private void btnThemNV_Click(object sender, EventArgs e)
        {
            bool validate = true;
            if (txtTenNV.Text == "" || txtTenDN.Text == "" || txtMK.Text == "" || txtMKLai.Text == "" || deNgaySinh.Text == "" || cbbGioiTinh.Text == "" || txtDiaChi.Text == "" || txtDT.Text == "")
                validate = false;
            if (validate == true)
            {
                //lưu xuống database
                if (txtMK.Text != txtMKLai.Text)
                    XtraMessageBox.Show(this, "Mật khẩu không trùng khớp", "Invalid Input", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else 
                {
                    //kiểm tra xong, lưu xuống database
                    if (bnv.bnvThemMoiNV(txtMaNV.Text, txtTenNV.Text, txtTenDN.Text, txtMK.Text, Convert.ToDateTime(deNgaySinh.Text), cbbGioiTinh.Text,
                        txtDiaChi.Text, txtDT.Text, cbbHocHam.GetItemText(cbbHocHam.SelectedValue).ToString(),
                        cbbHocVi.GetItemText(cbbHocVi.SelectedValue).ToString(), cbbNghiepVu.GetItemText(cbbNghiepVu.SelectedValue).ToString(),
                        cbbKhoa.GetItemText(cbbKhoa.SelectedValue).ToString(), pteAnhNV.Image))
                    {
                        //xử lý sau khi thêm nhân viên thành công
                        QuanLyBenhVien.frmQuanLyNhanSu.pqlnsInstance.gridControl1.DataSource = BangNhanVien.bnvLayThongTinNV();
                        txtTenNV.Text = txtTenDN.Text = txtMK.Text = txtMKLai.Text = txtDiaChi.Text = txtDT.Text = string.Empty;
                        cbbGioiTinh.SelectedIndex = cbbHocHam.SelectedIndex = cbbHocVi.SelectedIndex = cbbKhoa.SelectedIndex = cbbNghiepVu.SelectedIndex = 0;
                        pteAnhNV.Image = Properties.Resources.unknown;
                        deNgaySinh.EditValue = null;
                        string maCuoi = BangNhanVien.bnvLayMaNVCuoi();
                        string maMoi = "";
                        maMoi = HamTienIch.htiChuyenDoiMa2KyTu(maCuoi, "NV");
                        txtMaNV.Text = maMoi;
                        
                        XtraMessageBox.Show(this, "Thêm nhân viên thành công", "Thêm nhân viên", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else 
                    {
                        XtraMessageBox.Show("Có lỗi xảy ra!!");
                    }              
                }
            }
            else 
            {
                XtraMessageBox.Show(this , "Bạn chưa nhập đầy đủ thông tin hoặc thông tin nhập bị sai", "Invalid Input", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //thông báo lỗi
            }
        }
    }
}