﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraTab;

namespace QuanLyBenhVien
{
    public partial class frmTabmain : DevExpress.XtraEditors.XtraForm
    {
        public frmTabmain()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Xử lý sự kiện khi đóng tab
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xtraTabControl1_CloseButtonClick(object sender, EventArgs e)
        {
            try 
            {
                XtraTabControl mxtTab = (XtraTabControl)sender;
                int iChiSo = mxtTab.SelectedTabPageIndex;
                //dành cho xét điều kiện tab nào đc chọn

                //
                mxtTab.TabPages.RemoveAt(iChiSo);
                mxtTab.SelectedTabPageIndex = iChiSo - 1;
            }
            catch { }
        }
        /// <summary>
        /// Xử lý sự kiện khi chọn tab khác
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xtraTabControl1_SelectedPageChanged(object sender, TabPageChangedEventArgs e)
        {
            try 
            {
                if (xtraTabControl1.TabPages.Count > 0)
                {
                    QuanLyBenhVien.frmMainpage.pfrmInstance.mxtraTabControl.SelectedTabPageIndex = 0;
                }
                else 
                {
                    QuanLyBenhVien.frmMainpage.pfrmInstance.mxtraTabControl.SelectedTabPageIndex = 1;
                }
            }
            catch{}
        }
        
    }
}