﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using BLL;
using DevExpress.XtraGrid.Views.Base;

namespace QuanLyBenhVien
{
    public partial class frmQuanLyTTB : DevExpress.XtraEditors.XtraForm
    {
        BangTrangThietBi bttb = new BangTrangThietBi();
        public frmQuanLyTTB()
        {
            InitializeComponent();
        }

        private void mgvTTB_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            string maTrangThietBi = "";
            if (mgvTTB.RowCount == 0)
                maTrangThietBi = "TB001";
            else
            {
                maTrangThietBi = HamTienIch.htiChuyenDoiMa2KyTu(BangTrangThietBi.bttbLayMaTTBCuoi(), "TB");
            }
            ColumnView View = sender as ColumnView;
            View.SetRowCellValue(e.RowHandle, View.Columns["MaTrangThietBi"], maTrangThietBi);
        }

        private void mgvTTB_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            if (e.RowHandle < 0)
            {
                bttb.bttbThemTrangThietBi(mgvTTB.GetFocusedRowCellValue("MaTrangThietBi").ToString(), mgvTTB.GetFocusedRowCellValue("TenTrangThietBi").ToString(), (int)mgvTTB.GetFocusedRowCellValue("SoLuong"), mgvTTB.GetFocusedRowCellValue("MaDonViTrangThietBi").ToString());
                XtraMessageBox.Show("Thêm trang thiết bị thành công");
            }
            else
            {
                bttb.bttbSuaTrangThietBi(mgvTTB.GetFocusedRowCellValue("MaTrangThietBi").ToString(), mgvTTB.GetFocusedRowCellValue("TenTrangThietBi").ToString(), (int)mgvTTB.GetFocusedRowCellValue("SoLuong"), mgvTTB.GetFocusedRowCellValue("MaDonViTrangThietBi").ToString());
                XtraMessageBox.Show("Cập nhật trang thiết bị thành công");
            }
        }

        private void frmQuanLyTTB_Load(object sender, EventArgs e)
        {
            //load thong tin
            gridControl1.DataSource = BangTrangThietBi.bttbLayThongTin();
            mgvTTB.Columns[0].FieldName = "MaTrangThietBi";
            mgvTTB.Columns[1].FieldName = "TenTrangThietBi";
            mgvTTB.Columns[2].FieldName = "SoLuong";
            mgvTTB.Columns[3].FieldName = "MaDonViTrangThietBi";

            repositoryItemLookUpEdit1.DataSource = BangDonViTTB.bdvttbLayThongTin();
            repositoryItemLookUpEdit1.DisplayMember = "TenDonViTrangThietBi";
            repositoryItemLookUpEdit1.ValueMember = "MaDonViTrangThietBi";
        }

        private void txtTimKiem_TextChanged(object sender, EventArgs e)
        {
            if (txtTimKiem.Text != "")
            {
                switch (rdgTimTheo.Text)
                {
                    case "name":
                        mgvTTB.ApplyFindFilter("Tên:\"" + txtTimKiem.Text + "\"");
                        break;
                    case "dv":
                        mgvTTB.ApplyFindFilter("\"Đơn vị\":\"" + txtTimKiem.Text + "\"");
                        break;
                }
            }
            else
            {
                mgvTTB.ApplyFindFilter("");
            }
        }
    }
}