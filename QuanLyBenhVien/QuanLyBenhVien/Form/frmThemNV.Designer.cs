﻿namespace QuanLyBenhVien
{
    partial class frmThemNV
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.cbbKhoa = new System.Windows.Forms.ComboBox();
            this.cbbNghiepVu = new System.Windows.Forms.ComboBox();
            this.cbbHocVi = new System.Windows.Forms.ComboBox();
            this.cbbHocHam = new System.Windows.Forms.ComboBox();
            this.cbbGioiTinh = new System.Windows.Forms.ComboBox();
            this.btnXoaHinh = new DevExpress.XtraEditors.SimpleButton();
            this.btnThemNV = new DevExpress.XtraEditors.SimpleButton();
            this.btnThemHinh = new DevExpress.XtraEditors.SimpleButton();
            this.txtMKLai = new DevExpress.XtraEditors.TextEdit();
            this.txtDT = new DevExpress.XtraEditors.TextEdit();
            this.txtDiaChi = new DevExpress.XtraEditors.TextEdit();
            this.deNgaySinh = new DevExpress.XtraEditors.DateEdit();
            this.txtMK = new DevExpress.XtraEditors.TextEdit();
            this.txtTenDN = new DevExpress.XtraEditors.TextEdit();
            this.txtMaNV = new DevExpress.XtraEditors.TextEdit();
            this.txtTenNV = new DevExpress.XtraEditors.TextEdit();
            this.pteAnhNV = new DevExpress.XtraEditors.PictureEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleSeparator1 = new DevExpress.XtraLayout.SimpleSeparator();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMKLai.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiaChi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deNgaySinh.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deNgaySinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMK.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenDN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaNV.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenNV.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pteAnhNV.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.cbbKhoa);
            this.layoutControl1.Controls.Add(this.cbbNghiepVu);
            this.layoutControl1.Controls.Add(this.cbbHocVi);
            this.layoutControl1.Controls.Add(this.cbbHocHam);
            this.layoutControl1.Controls.Add(this.cbbGioiTinh);
            this.layoutControl1.Controls.Add(this.btnXoaHinh);
            this.layoutControl1.Controls.Add(this.btnThemNV);
            this.layoutControl1.Controls.Add(this.btnThemHinh);
            this.layoutControl1.Controls.Add(this.txtMKLai);
            this.layoutControl1.Controls.Add(this.txtDT);
            this.layoutControl1.Controls.Add(this.txtDiaChi);
            this.layoutControl1.Controls.Add(this.deNgaySinh);
            this.layoutControl1.Controls.Add(this.txtMK);
            this.layoutControl1.Controls.Add(this.txtTenDN);
            this.layoutControl1.Controls.Add(this.txtMaNV);
            this.layoutControl1.Controls.Add(this.txtTenNV);
            this.layoutControl1.Controls.Add(this.pteAnhNV);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(66, 141, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1300, 600);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // cbbKhoa
            // 
            this.cbbKhoa.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbKhoa.FormattingEnabled = true;
            this.cbbKhoa.Location = new System.Drawing.Point(462, 338);
            this.cbbKhoa.Name = "cbbKhoa";
            this.cbbKhoa.Size = new System.Drawing.Size(814, 21);
            this.cbbKhoa.TabIndex = 26;
            // 
            // cbbNghiepVu
            // 
            this.cbbNghiepVu.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbNghiepVu.FormattingEnabled = true;
            this.cbbNghiepVu.Location = new System.Drawing.Point(462, 313);
            this.cbbNghiepVu.Name = "cbbNghiepVu";
            this.cbbNghiepVu.Size = new System.Drawing.Size(814, 21);
            this.cbbNghiepVu.TabIndex = 25;
            // 
            // cbbHocVi
            // 
            this.cbbHocVi.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbHocVi.FormattingEnabled = true;
            this.cbbHocVi.Location = new System.Drawing.Point(462, 288);
            this.cbbHocVi.Name = "cbbHocVi";
            this.cbbHocVi.Size = new System.Drawing.Size(814, 21);
            this.cbbHocVi.TabIndex = 24;
            // 
            // cbbHocHam
            // 
            this.cbbHocHam.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbHocHam.FormattingEnabled = true;
            this.cbbHocHam.Location = new System.Drawing.Point(462, 263);
            this.cbbHocHam.Name = "cbbHocHam";
            this.cbbHocHam.Size = new System.Drawing.Size(814, 21);
            this.cbbHocHam.TabIndex = 23;
            // 
            // cbbGioiTinh
            // 
            this.cbbGioiTinh.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbGioiTinh.FormattingEnabled = true;
            this.cbbGioiTinh.Items.AddRange(new object[] {
            "Nam",
            "Nữ"});
            this.cbbGioiTinh.Location = new System.Drawing.Point(462, 190);
            this.cbbGioiTinh.Name = "cbbGioiTinh";
            this.cbbGioiTinh.Size = new System.Drawing.Size(814, 21);
            this.cbbGioiTinh.TabIndex = 22;
            // 
            // btnXoaHinh
            // 
            this.btnXoaHinh.Location = new System.Drawing.Point(101, 262);
            this.btnXoaHinh.Name = "btnXoaHinh";
            this.btnXoaHinh.Size = new System.Drawing.Size(102, 22);
            this.btnXoaHinh.StyleController = this.layoutControl1;
            this.btnXoaHinh.TabIndex = 21;
            this.btnXoaHinh.Text = "Xoá hình >>>";
            this.btnXoaHinh.Click += new System.EventHandler(this.btnXoaHinh_Click);
            // 
            // btnThemNV
            // 
            this.btnThemNV.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnThemNV.Appearance.Options.UseFont = true;
            this.btnThemNV.Image = global::QuanLyBenhVien.Properties.Resources.add;
            this.btnThemNV.Location = new System.Drawing.Point(1110, 558);
            this.btnThemNV.Name = "btnThemNV";
            this.btnThemNV.Size = new System.Drawing.Size(178, 30);
            this.btnThemNV.StyleController = this.layoutControl1;
            this.btnThemNV.TabIndex = 18;
            this.btnThemNV.Text = "Thêm nhân viên";
            this.btnThemNV.Click += new System.EventHandler(this.btnThemNV_Click);
            // 
            // btnThemHinh
            // 
            this.btnThemHinh.Location = new System.Drawing.Point(207, 262);
            this.btnThemHinh.Name = "btnThemHinh";
            this.btnThemHinh.Size = new System.Drawing.Size(111, 22);
            this.btnThemHinh.StyleController = this.layoutControl1;
            this.btnThemHinh.TabIndex = 17;
            this.btnThemHinh.Text = "Thêm hình>>>";
            this.btnThemHinh.Click += new System.EventHandler(this.btnThemHinh_Click);
            // 
            // txtMKLai
            // 
            this.txtMKLai.Location = new System.Drawing.Point(462, 142);
            this.txtMKLai.Name = "txtMKLai";
            this.txtMKLai.Properties.UseSystemPasswordChar = true;
            this.txtMKLai.Size = new System.Drawing.Size(814, 20);
            this.txtMKLai.StyleController = this.layoutControl1;
            this.txtMKLai.TabIndex = 16;
            this.txtMKLai.Validating += new System.ComponentModel.CancelEventHandler(this.txtEdit_Validating);
            // 
            // txtDT
            // 
            this.txtDT.Location = new System.Drawing.Point(462, 239);
            this.txtDT.Name = "txtDT";
            this.txtDT.Properties.Mask.EditMask = "0000 0000000";
            this.txtDT.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
            this.txtDT.Size = new System.Drawing.Size(814, 20);
            this.txtDT.StyleController = this.layoutControl1;
            this.txtDT.TabIndex = 12;
            this.txtDT.Validating += new System.ComponentModel.CancelEventHandler(this.txtEdit_Validating);
            // 
            // txtDiaChi
            // 
            this.txtDiaChi.Location = new System.Drawing.Point(462, 215);
            this.txtDiaChi.Name = "txtDiaChi";
            this.txtDiaChi.Size = new System.Drawing.Size(814, 20);
            this.txtDiaChi.StyleController = this.layoutControl1;
            this.txtDiaChi.TabIndex = 11;
            this.txtDiaChi.Validating += new System.ComponentModel.CancelEventHandler(this.txtEdit_Validating);
            // 
            // deNgaySinh
            // 
            this.deNgaySinh.EditValue = null;
            this.deNgaySinh.Location = new System.Drawing.Point(462, 166);
            this.deNgaySinh.Name = "deNgaySinh";
            this.deNgaySinh.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deNgaySinh.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deNgaySinh.Size = new System.Drawing.Size(814, 20);
            this.deNgaySinh.StyleController = this.layoutControl1;
            this.deNgaySinh.TabIndex = 9;
            this.deNgaySinh.Validating += new System.ComponentModel.CancelEventHandler(this.txtEdit_Validating);
            // 
            // txtMK
            // 
            this.txtMK.Location = new System.Drawing.Point(462, 118);
            this.txtMK.Name = "txtMK";
            this.txtMK.Properties.UseSystemPasswordChar = true;
            this.txtMK.Size = new System.Drawing.Size(814, 20);
            this.txtMK.StyleController = this.layoutControl1;
            this.txtMK.TabIndex = 8;
            this.txtMK.Validating += new System.ComponentModel.CancelEventHandler(this.txtEdit_Validating);
            // 
            // txtTenDN
            // 
            this.txtTenDN.Location = new System.Drawing.Point(462, 94);
            this.txtTenDN.Name = "txtTenDN";
            this.txtTenDN.Properties.Mask.EditMask = "\\w+\\d+\\p{P}+";
            this.txtTenDN.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtTenDN.Properties.Mask.ShowPlaceHolders = false;
            this.txtTenDN.Size = new System.Drawing.Size(814, 20);
            this.txtTenDN.StyleController = this.layoutControl1;
            this.txtTenDN.TabIndex = 7;
            this.txtTenDN.Validating += new System.ComponentModel.CancelEventHandler(this.txtEdit_Validating);
            // 
            // txtMaNV
            // 
            this.txtMaNV.Enabled = false;
            this.txtMaNV.Location = new System.Drawing.Point(462, 46);
            this.txtMaNV.Name = "txtMaNV";
            this.txtMaNV.Size = new System.Drawing.Size(814, 20);
            this.txtMaNV.StyleController = this.layoutControl1;
            this.txtMaNV.TabIndex = 6;
            // 
            // txtTenNV
            // 
            this.txtTenNV.CausesValidation = false;
            this.txtTenNV.Location = new System.Drawing.Point(462, 70);
            this.txtTenNV.Name = "txtTenNV";
            this.txtTenNV.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.txtTenNV.Properties.Mask.EditMask = "(\\p{L}+\\s?)+";
            this.txtTenNV.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtTenNV.Properties.Mask.ShowPlaceHolders = false;
            this.txtTenNV.Size = new System.Drawing.Size(814, 20);
            this.txtTenNV.StyleController = this.layoutControl1;
            this.txtTenNV.TabIndex = 5;
            this.txtTenNV.Validating += new System.ComponentModel.CancelEventHandler(this.txtEdit_Validating);
            // 
            // pteAnhNV
            // 
            this.pteAnhNV.EditValue = global::QuanLyBenhVien.Properties.Resources.unknown;
            this.pteAnhNV.Location = new System.Drawing.Point(12, 12);
            this.pteAnhNV.Name = "pteAnhNV";
            this.pteAnhNV.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pteAnhNV.Size = new System.Drawing.Size(306, 246);
            this.pteAnhNV.StyleController = this.layoutControl1;
            this.pteAnhNV.TabIndex = 4;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.layoutControlGroup1.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup1.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.layoutControlGroup1.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.simpleSeparator1,
            this.emptySpaceItem2,
            this.emptySpaceItem1,
            this.layoutControlItem14,
            this.emptySpaceItem3,
            this.layoutControlGroup2,
            this.emptySpaceItem5,
            this.layoutControlItem15,
            this.layoutControlItem18});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1300, 600);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.pteAnhNV;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(310, 250);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // simpleSeparator1
            // 
            this.simpleSeparator1.AllowHotTrack = false;
            this.simpleSeparator1.CustomizationFormText = "simpleSeparator1";
            this.simpleSeparator1.Location = new System.Drawing.Point(310, 0);
            this.simpleSeparator1.Name = "simpleSeparator1";
            this.simpleSeparator1.Size = new System.Drawing.Size(2, 580);
            this.simpleSeparator1.Text = "simpleSeparator1";
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(312, 546);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(786, 34);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 276);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(310, 304);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.btnThemHinh;
            this.layoutControlItem14.CustomizationFormText = "layoutControlItem14";
            this.layoutControlItem14.Location = new System.Drawing.Point(195, 250);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(115, 26);
            this.layoutControlItem14.Text = "layoutControlItem14";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem14.TextToControlDistance = 0;
            this.layoutControlItem14.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 250);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(89, 26);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "Thông tin nhân viên";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3,
            this.layoutControlItem2,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem13,
            this.layoutControlItem6,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem7,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem12,
            this.layoutControlItem16});
            this.layoutControlGroup2.Location = new System.Drawing.Point(312, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(968, 363);
            this.layoutControlGroup2.Text = "Thông tin nhân viên";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.txtMaNV;
            this.layoutControlItem3.CustomizationFormText = "Mã nhân viên :";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(944, 24);
            this.layoutControlItem3.Text = "Mã nhân viên :";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(123, 16);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.txtTenNV;
            this.layoutControlItem2.CustomizationFormText = "Tên nhân viên :";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(944, 24);
            this.layoutControlItem2.Text = "Tên nhân viên :";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(123, 16);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.txtTenDN;
            this.layoutControlItem4.CustomizationFormText = "Tên đăng nhập :";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(944, 24);
            this.layoutControlItem4.Text = "Tên đăng nhập :";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(123, 16);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.txtMK;
            this.layoutControlItem5.CustomizationFormText = "Mật khẩu :";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(944, 24);
            this.layoutControlItem5.Text = "Mật khẩu :";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(123, 16);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.txtMKLai;
            this.layoutControlItem13.CustomizationFormText = "Nhập lại mật khẩu :";
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(944, 24);
            this.layoutControlItem13.Text = "Nhập lại mật khẩu :";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(123, 16);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.deNgaySinh;
            this.layoutControlItem6.CustomizationFormText = "Ngày sinh :";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(944, 24);
            this.layoutControlItem6.Text = "Ngày sinh :";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(123, 16);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.txtDiaChi;
            this.layoutControlItem8.CustomizationFormText = "Địa chỉ :";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 169);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(944, 24);
            this.layoutControlItem8.Text = "Địa chỉ :";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(123, 16);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.txtDT;
            this.layoutControlItem9.CustomizationFormText = "Điện thoại :";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 193);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(944, 24);
            this.layoutControlItem9.Text = "Điện thoại :";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(123, 16);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.cbbGioiTinh;
            this.layoutControlItem7.CustomizationFormText = "Giới tính :";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 144);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(944, 25);
            this.layoutControlItem7.Text = "Giới tính :";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(123, 16);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.cbbHocHam;
            this.layoutControlItem10.CustomizationFormText = "Học hàm :";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 217);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(944, 25);
            this.layoutControlItem10.Text = "Học hàm :";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(123, 16);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.cbbHocVi;
            this.layoutControlItem11.CustomizationFormText = "Học vị :";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 242);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(944, 25);
            this.layoutControlItem11.Text = "Học vị :";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(123, 16);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.cbbNghiepVu;
            this.layoutControlItem12.CustomizationFormText = "Nghiệp vụ :";
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 267);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(944, 25);
            this.layoutControlItem12.Text = "Ngiệp vụ :";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(123, 16);
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.cbbKhoa;
            this.layoutControlItem16.CustomizationFormText = "Khoa :";
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 292);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(944, 25);
            this.layoutControlItem16.Text = "Khoa :";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(123, 16);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(312, 363);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(968, 183);
            this.emptySpaceItem5.Text = "emptySpaceItem5";
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.btnThemNV;
            this.layoutControlItem15.CustomizationFormText = "layoutControlItem15";
            this.layoutControlItem15.Location = new System.Drawing.Point(1098, 546);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(182, 34);
            this.layoutControlItem15.Text = "layoutControlItem15";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem15.TextToControlDistance = 0;
            this.layoutControlItem15.TextVisible = false;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.btnXoaHinh;
            this.layoutControlItem18.CustomizationFormText = "layoutControlItem18";
            this.layoutControlItem18.Location = new System.Drawing.Point(89, 250);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(106, 26);
            this.layoutControlItem18.Text = "layoutControlItem18";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem18.TextToControlDistance = 0;
            this.layoutControlItem18.TextVisible = false;
            // 
            // frmThemNV
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1300, 600);
            this.Controls.Add(this.layoutControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmThemNV";
            this.Text = "frmThemNV";
            this.Load += new System.EventHandler(this.frmThemNV_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtMKLai.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiaChi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deNgaySinh.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deNgaySinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMK.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenDN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaNV.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenNV.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pteAnhNV.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.TextEdit txtMaNV;
        private DevExpress.XtraEditors.TextEdit txtTenNV;
        private DevExpress.XtraEditors.PictureEdit pteAnhNV;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.TextEdit txtTenDN;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.TextEdit txtMK;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.DateEdit deNgaySinh;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraEditors.TextEdit txtDiaChi;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.TextEdit txtDT;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraEditors.TextEdit txtMKLai;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraEditors.SimpleButton btnThemHinh;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraEditors.SimpleButton btnThemNV;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraEditors.SimpleButton btnXoaHinh;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private System.Windows.Forms.ComboBox cbbKhoa;
        private System.Windows.Forms.ComboBox cbbNghiepVu;
        private System.Windows.Forms.ComboBox cbbHocVi;
        private System.Windows.Forms.ComboBox cbbHocHam;
        private System.Windows.Forms.ComboBox cbbGioiTinh;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
    }
}