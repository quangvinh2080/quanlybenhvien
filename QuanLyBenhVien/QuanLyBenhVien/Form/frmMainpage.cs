﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BLL;
using DAL;
using DevExpress.XtraBars.Localization;
using DevExpress.XtraBars.Helpers;
using DevExpress.XtraSplashScreen;
using DevExpress.XtraGrid.Localization;
using DevExpress.XtraEditors;

namespace QuanLyBenhVien
{
    public partial class frmMainpage : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public static frmMainpage pfrmInstance;

        public frmDangNhap frm_DangNhap;
        public frmTabmain frm_Tabmain;

        public frmMainpage()
        {
            InitializeComponent();
            qlKhoiTao();
            pfrmInstance = this;
        }

        private void qlKhoiTao()
        {
            frm_DangNhap = new frmDangNhap();
            frm_Tabmain = new frmTabmain();
        }

        private void frmMainpage_Load(object sender, EventArgs e)
        {
            BarLocalizer.Active = new MyBarLocalizer();
            GridLocalizer.Active = new MyGridLocalizer();
            SkinHelper.InitSkinGallery(ribbonGalleryBarItem1, true);
            //DevExpress.LookAndFeel.UserLookAndFeel.Default.SetSkinStyle("Blue");//Nằm trong bảng THAMSO trong database
            DieuKhien.dkTaoTab(mxtraTabControl, "", frm_Tabmain.Name, frm_Tabmain);
            DieuKhien.dkTaoTab(mxtraTabControl, "", frm_DangNhap.Name, frm_DangNhap);
        }

        private void mbtnTiepNhan_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            SplashScreenManager.ShowForm(typeof(WaitForm1));
            frmTiepNhanBenhNhan frm_TiepNhanBenhNhan = new frmTiepNhanBenhNhan();

            DieuKhien.dkTaoTab(frm_Tabmain.xtraTabControl1, "Tiếp nhận BN", frm_TiepNhanBenhNhan.Name, frm_TiepNhanBenhNhan);
            SplashScreenManager.CloseForm();
        }


        private void btnKhamBenh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            SplashScreenManager.ShowForm(typeof(WaitForm1));
            frmKhamBenh frm_KhamBenh = new frmKhamBenh();

            DieuKhien.dkTaoTab(frm_Tabmain.xtraTabControl1, "Khám bệnh", frm_KhamBenh.Name, frm_KhamBenh);
            SplashScreenManager.CloseForm();
        }

        private void mbtnQuanLyNS_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            SplashScreenManager.ShowForm(typeof(WaitForm1));
            frmQuanLyNhanSu frm_QuanLyNhanSu = new frmQuanLyNhanSu();

            DieuKhien.dkTaoTab(frm_Tabmain.xtraTabControl1, "Quản lý nhân sự", frm_QuanLyNhanSu.Name, frm_QuanLyNhanSu);
            SplashScreenManager.CloseForm();
        }

        private void mbtnThuPhi_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            SplashScreenManager.ShowForm(typeof(WaitForm1));
            frmThuPhi frm_ThuPhi = new frmThuPhi();
            DieuKhien.dkTaoTab(frm_Tabmain.xtraTabControl1, "Thu phí", frm_ThuPhi.Name, frm_ThuPhi);
            SplashScreenManager.CloseForm();
        }

        private void mbtnCauHinh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            SplashScreenManager.ShowForm(typeof(WaitForm1));
            frmCauHinh frm_CauHinh = new frmCauHinh();
            DieuKhien.dkTaoTab(frm_Tabmain.xtraTabControl1, "Cấu hình hệ thống", frm_CauHinh.Name, frm_CauHinh);
            SplashScreenManager.CloseForm();
        }

        private void mbtnThuoc_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            SplashScreenManager.ShowForm(typeof(WaitForm1));
            frmQuanLyThuoc frm_QuanLyThuoc = new frmQuanLyThuoc();
            DieuKhien.dkTaoTab(frm_Tabmain.xtraTabControl1, "Quản lý thuốc", frm_QuanLyThuoc.Name, frm_QuanLyThuoc);
            SplashScreenManager.CloseForm();
        }

        private void mbtnTtb_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            SplashScreenManager.ShowForm(typeof(WaitForm1));
            frmQuanLyTTB frm_QuanLyTTB = new frmQuanLyTTB();
            DieuKhien.dkTaoTab(frm_Tabmain.xtraTabControl1, "Quản lý TTB", frm_QuanLyTTB.Name, frm_QuanLyTTB);
            SplashScreenManager.CloseForm();
        }

        private void mbtnQuanLyBL_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            SplashScreenManager.ShowForm(typeof(WaitForm1));
            frmQuanLyBenhLy frm_QuanLyBenhLy = new frmQuanLyBenhLy();
            DieuKhien.dkTaoTab(frm_Tabmain.xtraTabControl1, "Quản lý bệnh lý", frm_QuanLyBenhLy.Name, frm_QuanLyBenhLy);
            SplashScreenManager.CloseForm();
        }

        private void mbtnQuanLyVP_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            SplashScreenManager.ShowForm(typeof(WaitForm1));
            frmQuanLyVienPhi frm_QuanLyVienPhi = new frmQuanLyVienPhi();
            DieuKhien.dkTaoTab(frm_Tabmain.xtraTabControl1, "Quản lý viện phí", frm_QuanLyVienPhi.Name, frm_QuanLyVienPhi);
            SplashScreenManager.CloseForm();
        }

        private void mbtnQuanLyCLS_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            SplashScreenManager.ShowForm(typeof(WaitForm1));
            frmQuanLyCLS frm_QuanLyCLS = new frmQuanLyCLS();
            DieuKhien.dkTaoTab(frm_Tabmain.xtraTabControl1, "Quản lý CLS", frm_QuanLyCLS.Name, frm_QuanLyCLS);
            SplashScreenManager.CloseForm();
        }

        private void mbtnDangNhap_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DieuKhien.dkTaoTab(mxtraTabControl, "Đăng nhập", frm_DangNhap.Name, frm_DangNhap);
        }
        /// <summary>
        /// Tuỳ chỉnh enable của button
        /// </summary>
        /// <param name="tenButton"></param>
        /// <param name="trangthai"></param>
        public void mpTrangThaiButton(string tenButton, bool trangthai)
        {
            switch (tenButton)
            { 
                case "mbtnTiepNhan":
                    mbtnTiepNhan.Enabled = trangthai;
                    break;
                case "mbtnThuPhi":
                    mbtnThuPhi.Enabled = trangthai;
                    break;
                case "mbtnKhamBenh":
                    mbtnKhamBenh.Enabled = trangthai;
                    break;
                case "mbtnBaoCaoTon":
                    mbtnBaoCaoTon.Enabled = trangthai;
                    break;
                case "mbtnBaoCaoDoanhThu":
                    mbtnBaoCaoDoanhThu.Enabled = trangthai;
                    break;
                case "mbtnDangNhap":
                    mbtnDangNhap.Enabled = trangthai;
                    break;
                case "mbtnDangXuat":
                    mbtnDangXuat.Enabled = trangthai;
                    break;
                case "mbtnThayDoiMK":
                    mbtnThayDoiMK.Enabled = trangthai;
                    break;
                case "mbtnCauHinh":
                    mbtnCauHinh.Enabled = trangthai;
                    break;
                case "mbtnQuanlyNS":
                    mbtnQuanLyNS.Enabled = trangthai;
                    break;
                case "mbtnThuoc":
                    mbtnThuoc.Enabled = trangthai;
                    break;
                case "mbtnTtb":
                    mbtnTtb.Enabled = trangthai;
                    break;
                case "mbtnQuanLyBL":
                    mbtnQuanLyBL.Enabled = trangthai;
                    break;
                case "mbtnQuanLyVP":
                    mbtnQuanLyVP.Enabled = trangthai;
                    break;
                case "mbtnQuanLyCLS":
                    mbtnQuanLyCLS.Enabled = trangthai;
                    break;
                //Thêm button mới phải thêm case vào đây
            }
        }
        /// <summary>
        /// Thay đổi trạng thái tất cả button trừ đăng nhập, đăng xuất, thay đổi mk
        /// </summary>
        /// <param name="trangthai"></param>
        public void mpThayDoiTrangThaiTatCaButton(bool trangthai)
        {
            mbtnTiepNhan.Enabled = trangthai;
            mbtnKhamBenh.Enabled = trangthai;
            mbtnThuPhi.Enabled = trangthai;
            mbtnBaoCaoTon.Enabled = trangthai;
            mbtnBaoCaoDoanhThu.Enabled = trangthai;
            mbtnCauHinh.Enabled = trangthai;
            mbtnQuanLyNS.Enabled = trangthai;
            mbtnThuoc.Enabled = trangthai;
            mbtnTtb.Enabled = trangthai;
            mbtnQuanLyBL.Enabled = trangthai;
            mbtnQuanLyVP.Enabled = trangthai;
            mbtnQuanLyCLS.Enabled = trangthai;
        }
        /// <summary>
        /// Thay đổi trạng thái 3 button đăng nhập đăng xuất thay đổi mk (khi đăng nhập)
        /// </summary>
        public void mpThayDoiTrangThaiDangNhap() 
        {
            mbtnDangNhap.Enabled = false;
            mbtnDangXuat.Enabled = true;
            mbtnThayDoiMK.Enabled = true;
        }
        /// <summary>
        /// Thay đổi trạng thái 3 button đăng nhập đăng xuất thay đổi mk (khi đăng xuất)
        /// </summary>
        public void mpThayDoiTrangThaiDangXuat() 
        {
            mbtnDangNhap.Enabled = true;
            mbtnDangXuat.Enabled = false;
            mbtnThayDoiMK.Enabled = false;
        }
        /// <summary>
        /// Thay đổi chuỗi hiển thị tên người dùng
        /// </summary>
        /// <param name="tenNguoiDung"></param>
        public void mpThayDoiTenNguoiDung(string tenNguoiDung) 
        {
            mtxtTenNhanVien.Caption = tenNguoiDung;
        }
        private void mbtnDangXuat_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (XtraMessageBox.Show("Tât cả các cửa sổ đang sẽ bị đóng lại.\nBạn có thực sự muốn đăng xuất?", "Đăng Xuất", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK) 
            {
                mtxtTenNhanVien.Caption = "Khách";
                mpDongTatCaCuaSo();
                mpThayDoiTrangThaiDangXuat();
                mpThayDoiTrangThaiTatCaButton(false);
                
                mbtnDangNhap.PerformClick();
            }
        }
        /// <summary>
        /// Đóng hết tất cả cửa sổ
        /// </summary>
        private void mpDongTatCaCuaSo() 
        {
            while (frm_Tabmain.xtraTabControl1.TabPages.Count>0)
            {
                frm_Tabmain.xtraTabControl1.TabPages.RemoveAt(0);
            }
        }

        private void mbtnThayDoiMK_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmThayDoiMK frm_ThayDoiMK = new frmThayDoiMK();
            frm_ThayDoiMK.ShowDialog();
        }
    }
}
