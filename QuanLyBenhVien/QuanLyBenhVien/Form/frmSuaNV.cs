﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.IO;
using BLL;

namespace QuanLyBenhVien
{
    public partial class frmSuaNV : DevExpress.XtraEditors.XtraForm
    {
        BangNhanVien bnv = new BangNhanVien();
        private string maNV = "";
        public frmSuaNV(string _maNV)
        {
            InitializeComponent();
            maNV = _maNV;
        }

        private void frmSuaNV_Load(object sender, EventArgs e)
        {
            //Load thông tin combobox;
            
            cbbHocVi.DataSource = BangHocVi.bhvLayDsHocVi();
            cbbHocVi.DisplayMember = "TenHocVi";
            cbbHocVi.ValueMember = "MaHocVi";

            cbbHocHam.DataSource = BangHocHam.bhhLayDsHocHam();
            cbbHocHam.DisplayMember = "TenHocHam";
            cbbHocHam.ValueMember = "MaHocHam";

            cbbNghiepVu.DataSource = BangNghiepVu.bnvLayDsNghiepVu();
            cbbNghiepVu.DisplayMember = "TenNghiepVu";
            cbbNghiepVu.ValueMember = "MaNghiepVu";

            cbbKhoa.DataSource = BangKhoa.bkLayThongTin();
            cbbKhoa.DisplayMember = "TenKhoa";
            cbbKhoa.ValueMember = "MaKhoa";
            
            //load thông tin nhân viên
            bnv = BangNhanVien.bnvLayThongTinNV(maNV);

            txtMaNV.Text = bnv.Rows[0][1].ToString();//mã nhân viên
            byte[] byteImg = (byte[])bnv.Rows[0][0];
            pteAnhNV.Image = HinhAnh.haChuyenByteSangAnh(byteImg);//ảnh
            txtTenNV.Text = bnv.Rows[0][2].ToString();//tên nhân viên
            txtTenDN.Text = bnv.Rows[0][3].ToString();//tên đăng nhập
            txtMK.Text = bnv.Rows[0][4].ToString();//mật khẩu
            deNgaySinh.EditValue = bnv.Rows[0][5];//ngày sinh
            cbbGioiTinh.Text = bnv.Rows[0][6].ToString();//giới tính
            txtDiaChi.Text = bnv.Rows[0][7].ToString();//dia chi
            txtDT.Text = bnv.Rows[0][8].ToString();//dien thoai

            //hoc ham
            cbbHocHam.SelectedValue = bnv.Rows[0][9].ToString();
            //hoc vi
            cbbHocVi.SelectedValue = bnv.Rows[0][10].ToString();
            //nghiep vu
            cbbNghiepVu.SelectedValue = bnv.Rows[0][11].ToString();
            //khoa
            cbbKhoa.SelectedValue = bnv.Rows[0][12].ToString();
        }

        private void btnXoaHinh_Click(object sender, EventArgs e)
        {
            pteAnhNV.Image = Properties.Resources.unknown;
        }
        private void txtEdit_Validating(object sender, CancelEventArgs e)
        {
            TextEdit view = sender as TextEdit;
            if (view.Text == "")
            {
                e.Cancel = true;
                view.ErrorText = "Trường này không được để trống";
            }
            else
            {
                e.Cancel = false;
                view.ErrorText = "";
            }

        }
        private void btnCapNhat_Click(object sender, EventArgs e)
        {
            bool validate = true;
            if (txtTenNV.Text == "" || txtTenDN.Text == "" || txtMK.Text == "" || deNgaySinh.Text == "" || cbbGioiTinh.Text == "" || txtDiaChi.Text == "" || txtDT.Text == "")
                validate = false;
            if (validate == true)
            {
                if (bnv.bnvSuaThongTinNV(txtMaNV.Text, txtTenNV.Text, txtTenDN.Text, txtMK.Text, Convert.ToDateTime(deNgaySinh.Text), cbbGioiTinh.Text,
                        txtDiaChi.Text, txtDT.Text, cbbHocHam.GetItemText(cbbHocHam.SelectedValue).ToString(),
                        cbbHocVi.GetItemText(cbbHocVi.SelectedValue).ToString(), cbbNghiepVu.GetItemText(cbbNghiepVu.SelectedValue).ToString(),
                        cbbKhoa.GetItemText(cbbKhoa.SelectedValue).ToString(), pteAnhNV.Image))
                {
                    //xử lý sau khi cập nhật thành công
                    XtraMessageBox.Show(this, "Cập nhật thông tin nhân viên thành công", "Sửa nhân viên", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else 
                {
                    XtraMessageBox.Show("Có lỗi xảy ra!!");
                }
            }
            else
            {
                XtraMessageBox.Show(this, "Bạn chưa nhập đầy đủ thông tin hoặc thông tin nhập bị sai", "Invalid Input", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //thông báo lỗi
            }
        }

        private void btnSuaHinh_Click(object sender, EventArgs e)
        {
            OpenFileDialog mOfd = new OpenFileDialog();
            mOfd.Filter = "Image Files()|*.jpg|Image Bimap()|*.gif|Image png()|*.png";
            try
            {
                if (mOfd.ShowDialog() == DialogResult.OK)
                {
                    if (File.Exists(mOfd.FileName))
                    {
                        pteAnhNV.Image = Image.FromFile(mOfd.FileName);//ChinhAnh.pfncResizeAnh(mOfd.FileName, mptbAnhNV.Height, mptbAnhNV.Width);
                    }
                }
            }
            catch { }
        }
    }
}