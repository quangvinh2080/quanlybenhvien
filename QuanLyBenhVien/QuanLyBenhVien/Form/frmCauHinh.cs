﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using BLL;
using DevExpress.XtraGrid.Views.Base;

namespace QuanLyBenhVien
{
    public partial class frmCauHinh : DevExpress.XtraEditors.XtraForm
    {
        BangKhoa bk = new BangKhoa();
        BangPhongKham bpk = new BangPhongKham();
        public frmCauHinh()
        {
            InitializeComponent();
        }

        private void frmCauHinh_Load(object sender, EventArgs e)
        {
            loadThongTinBenhVien();
            gridControl1.DataSource = BangKhoa.bkLayThongTin();
            mgvKhoa.Columns[0].FieldName = "MaKhoa";
            mgvKhoa.Columns[1].FieldName = "TenKhoa";

            //load phong kham
            gridControl2.DataSource = BangPhongKham.bpkLayThongTin();
            mgvPhongKham.Columns[0].FieldName = "MaPhongKham";
            mgvPhongKham.Columns[1].FieldName = "TenPhongKham";
            mgvPhongKham.Columns[2].FieldName = "MaNV";

            //colMaNV_PK
            repositoryItemLookUpEdit1.DataSource = BangNhanVien.bnvLayMaTenVaKhoaNV();
            repositoryItemLookUpEdit1.DisplayMember = "HoTenNV";
            repositoryItemLookUpEdit1.ValueMember = "MaNV";
        }

        private void loadThongTinBenhVien()
        {
            txtTenBV.Text = BangThamSo.btsTenBenhVien();
            txtDiaChi.Text = BangThamSo.btsDiaChi();
            txtSoDT.Text = BangThamSo.btsSoDT();
            txtSoDD.Text = BangThamSo.btsSoDD();
        }

        private void mgvKhoa_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            if (e.RowHandle < 0)
            {
                bk.bkThemKhoaMoi(mgvKhoa.GetFocusedRowCellValue("MaKhoa").ToString(), mgvKhoa.GetFocusedRowCellValue("TenKhoa").ToString());
                XtraMessageBox.Show("Thêm khoa thành công");
            }
            else
            {
                bk.bkSuaThongTinKhoa(mgvKhoa.GetRowCellValue(e.RowHandle, "MaKhoa").ToString(), mgvKhoa.GetRowCellValue(e.RowHandle, "TenKhoa").ToString());
                XtraMessageBox.Show("Cập nhật thành công");
            }
        }

        private void mgvKhoa_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            string maKhoaMoi = HamTienIch.htiChuyenDoiMa1KyTu(BangKhoa.bkLayMaKhoaCuoi(), "K");
            ColumnView View = sender as ColumnView;
            View.SetRowCellValue(e.RowHandle, View.Columns["MaKhoa"], maKhoaMoi);
        }

        private void mgvPhongKham_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            string maPhongKhamMoi = HamTienIch.htiChuyenDoiMa2KyTu(BangPhongKham.bpkLayMaPKCuoi(), "PK");
            ColumnView View = sender as ColumnView;
            View.SetRowCellValue(e.RowHandle, View.Columns["MaPhongKham"], maPhongKhamMoi);
        }

        private void mgvPhongKham_RowUpdated(object sender, RowObjectEventArgs e)
        {
            if (e.RowHandle < 0)
            {
                bpk.bpkThemPKMoi(mgvPhongKham.GetFocusedRowCellValue("MaPhongKham").ToString(), mgvPhongKham.GetFocusedRowCellValue("TenPhongKham").ToString(), mgvPhongKham.GetFocusedRowCellValue("MaNV").ToString());
                XtraMessageBox.Show("Thêm phòng khám thành công");
            }
            else
            {
                bpk.bpkSuaThongTinPK(mgvPhongKham.GetFocusedRowCellValue("MaPhongKham").ToString(), mgvPhongKham.GetFocusedRowCellValue("TenPhongKham").ToString(), mgvPhongKham.GetFocusedRowCellValue("MaNV").ToString());
                XtraMessageBox.Show("Cập nhật thành công");
            }
        }

    }
}