﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;

namespace DAL
{
    public class ConnectDatabase : DataTable
    {
        private SqlConnection msqlCnConnection = null;
        private SqlDataAdapter msqladptAdapter = null;
        private SqlCommand msqCmCommand = null;
        private string mstrTruyVan = null;
        /// <summary>
        /// Chuỗi Truy Vấn
        /// </summary>
        public string ChuoiTruyVan
        {
            get
            {
                return mstrTruyVan;
            }
            set
            {
                mstrTruyVan = value;
            }
        }
        /// <summary>
        /// SqlConnection
        /// </summary>
        public SqlConnection Connection
        {
            get
            {
                return msqlCnConnection;
            }
            set
            {
                msqlCnConnection = value;
            }

        }
        /// <summary>
        /// Khởi Tạo
        /// </summary>
        public ConnectDatabase()
            : base()
        {

        }
        /// <summary>
        /// Khởi Tạo
        /// </summary>
        /// <param name="chuoiTruyVan">Chuỗi Truy Vấn</param>
        public ConnectDatabase(string chuoiTruyVan)
        {
            mstrTruyVan = chuoiTruyVan;
            cdDocBang();
        }
        /// <summary>
        /// Tạo Kết Nối
        /// </summary>
        public void cdTaoKetNoi()
        {
            try
            {
                Connection = new SqlConnection(@"Data Source=(local);Initial Catalog=QuanLyBenhVien;Integrated Security=True");

                if (Connection.State == ConnectionState.Closed)
                    Connection.Open();
            }
            catch { }

        }
        /// <summary>
        /// Đóng Kết Nối
        /// </summary>
        public void cdDongKetNoi()
        {
            try
            {
                if (Connection.State == ConnectionState.Open)
                    Connection.Close();
            }
            catch
            {
                MessageBox.Show("Có lỗi khi ngắt kết nối với dữ liệu.", "Kết Nối", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        /// <summary>
        /// Đọc Dữ Liệu Từ Bảng
        /// </summary>
        public void cdDocBang()
        {
            cdTaoKetNoi();

            try
            {
                msqCmCommand = Connection.CreateCommand();
                msqCmCommand.CommandText = mstrTruyVan;
                msqladptAdapter = new SqlDataAdapter(msqCmCommand);
                this.Clear();
                msqladptAdapter.Fill(this);
                cdDongKetNoi();
            }
            catch { }
        }
        /// <summary>
        /// Thực Hiện Câu Truy Vấn
        /// </summary>
        /// <param name="strStoreProcedure"></param>
        /// <param name="sqlParameter"></param>
        /// <returns></returns>
        public bool cdStoreProceduce(string strStoreProcedure, SqlParameter[] sqlParameter)
        {
            try
            {
                cdTaoKetNoi();

                SqlCommand sqlcom = new SqlCommand(strStoreProcedure, Connection);
                sqlcom.CommandType = CommandType.StoredProcedure;
                if (sqlParameter != null)
                    foreach (SqlParameter s1 in sqlParameter)
                    {
                        sqlcom.Parameters.Add(s1);
                    }
                sqlcom.ExecuteNonQuery();
                cdDongKetNoi();
                return true;
            }
            catch
            {
                cdDongKetNoi();
                return false;
            }
        }

    }
}