﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;

namespace BLL
{
    public class BangHocVi : ConnectDatabase
    {
        public BangHocVi() { }
        public BangHocVi(string query) : base(query) { }

        public static BangHocVi bhvLayDsHocVi()
        {
            return new BangHocVi("select * from HOCVI;");
        }
    }
}
