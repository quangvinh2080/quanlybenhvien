﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Windows.Forms;
using DevExpress.XtraTab;

namespace BLL
{
    public class DieuKhien
    {
        /// <summary>
        /// Tạo Mới Tab Điều Khiển
        /// </summary>
        /// <param name="xTabControl">XtraTabControl Cần Thêm</param>
        /// <param name="strText">Text</param>
        /// <param name="strTen">Tên</param>
        /// <param name="xForm">Form</param>
        public static void dkTaoTab(XtraTabControl xTabControl, string strText, string strTen, DevExpress.XtraEditors.XtraForm xForm) 
        {
            int miChiSo = dkKiemTraTonTai(xTabControl, strTen);

            try
            {
                if (miChiSo >= 0)
                {
                    xTabControl.SelectedTabPage = xTabControl.TabPages[miChiSo];
                    xTabControl.SelectedTabPage.Text = strText;
                }
                else
                {
                    XtraTabPage mTabPage = new XtraTabPage { Text = strText, Name = strTen };

                    xTabControl.TabPages.Add(mTabPage);
                    xTabControl.SelectedTabPage = mTabPage;
                    xForm.TopLevel = false;
                    xForm.Parent = mTabPage;
                    xForm.Show();
                    xForm.Dock = DockStyle.Fill;
                }
            }
            catch { }
        }

        private static int dkKiemTraTonTai(XtraTabControl xTabControl, string strTenTab)
        {
            int miKqua = -1;

            try
            {
                for (int i = 0; i < xTabControl.TabPages.Count; i++)
                {
                    if (xTabControl.TabPages[i].Name == strTenTab)
                    {
                        miKqua = i;
                        break;
                    }
                }
            }
            catch { }

            return miKqua;
        }
    }
}
