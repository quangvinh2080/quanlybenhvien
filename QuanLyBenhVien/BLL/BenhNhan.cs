﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data;



namespace QuanLyBenhVien
{
    public class BenhNhan
    {
        ConnectDatabase connect;

        //Lay Danh Sach Benh nhan cho kham
        public DataTable getDanhSachChoKham()
        {
            String procedureName = "LayDanhSachCho";
            connect = new ConnectDatabase();
            return connect.pfncStoreProcedureFillTable(procedureName, null);
        }
    }
}
