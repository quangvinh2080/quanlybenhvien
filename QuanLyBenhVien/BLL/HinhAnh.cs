﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.IO;

namespace BLL
{
    public class HinhAnh
    {
        public static byte[] haChuyenAnhSangByte(Image imgAnh)
        {
            MemoryStream msStream = new MemoryStream();
            imgAnh.Save(msStream, ImageFormat.Bmp);
            return msStream.ToArray();
        }

        public static Image haChuyenByteSangAnh(byte[] bytes)
        {
            MemoryStream memStream = new MemoryStream(bytes);
            return Image.FromStream(memStream);
        }
    }
}
