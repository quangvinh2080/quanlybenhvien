﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using DAL;

namespace BLL
{
    public class BangThamSo : ConnectDatabase
    {
        public BangThamSo() { }

        public BangThamSo(string tenThamSo)
            : base("select GiaTriThamSo from THAMSO where THAMSO.TenThamSo = '" + tenThamSo + "';")
        { 
            
        }

        public static string btsTenBenhVien() 
        {
            return new BangThamSo("TenBenhVien").Rows[0][0].ToString();
        }

        public static string btsDiaChi()
        {
            return new BangThamSo("DiaChi").Rows[0][0].ToString();
        }

        public static string btsSoDT()
        {
            return new BangThamSo("SoDienThoai").Rows[0][0].ToString();
        }

        public static string btsSoDD()
        {
            return new BangThamSo("SoDiDong").Rows[0][0].ToString();
        }
    }
}
