﻿using DAL;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using DAL;
namespace BLL
{
    public class BangKhoa : ConnectDatabase
    {
        public BangKhoa() { }
        public BangKhoa(string query) : base(query) { }

        public static BangKhoa bkLayThongTin()
        {
            return new BangKhoa("select * from KHOA;");
        }

        public static string bkLayMaKhoaCuoi()
        {
            BangKhoa bk = new BangKhoa("select TOP 1 MaKhoa from KHOA ORDER BY MaKhoa DESC");
            if (bk.Rows.Count > 0)
                return bk.Rows[0][0].ToString();
            else
                return null;
        }

        public bool bkThemKhoaMoi(string maKhoa, string tenKhoa)
        {
            SqlParameter[] sqlParameters = new SqlParameter[2];
            sqlParameters[0] = new SqlParameter("@MaKhoa", maKhoa);
            sqlParameters[1] = new SqlParameter("@TenKhoa", tenKhoa);
            if (cdStoreProceduce("spThemKhoa", sqlParameters))
                return true;
            else
                return false;
        }

        public bool bkSuaThongTinKhoa(string maKhoa, string tenKhoa)
        {
            SqlParameter[] sqlParameters = new SqlParameter[2];
            sqlParameters[0] = new SqlParameter("@MaKhoa", maKhoa);
            sqlParameters[1] = new SqlParameter("@TenKhoa", tenKhoa);
            if (cdStoreProceduce("spSuaKhoa", sqlParameters))
                return true;
            else
                return false;
        }
    }
}
