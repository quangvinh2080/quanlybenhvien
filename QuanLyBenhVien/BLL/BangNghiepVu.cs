﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;

namespace BLL
{
    public class BangNghiepVu : ConnectDatabase
    {
        public BangNghiepVu() { }

        public BangNghiepVu(string query) : base(query) { }
        public static string bnvLayTenNghiepVu(string maNghiepVu)
        {
            return new BangNghiepVu("select TenNghiepVu from NGHIEPVU where MaNghiepVu = '" + maNghiepVu + "';").Rows[0][0].ToString();
        }

        public static BangNghiepVu bnvLayDsNghiepVu()
        {
            return new BangNghiepVu("select * from NGHIEPVU;");
        }
    }
}
