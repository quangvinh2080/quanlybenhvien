﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;

namespace BLL
{
    public class BangHocHam : ConnectDatabase
    {
        public BangHocHam() { }
        public BangHocHam(string query) : base(query) { }

        public static BangHocHam bhhLayDsHocHam()
        {
            return new BangHocHam("select * from HOCHAM;");
        }
    }
}
