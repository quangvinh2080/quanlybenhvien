﻿using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;

namespace BLL
{
    public class BangNhanVien : ConnectDatabase
    {
        public BangNhanVien() { }

        public BangNhanVien(string query) : base(query) { }

        public static string bnvLayMaNVCuoi()
        {
            BangNhanVien bnv = new BangNhanVien("select TOP 1 MaNV from NHANVIEN ORDER BY MaNV DESC");
            if (bnv.Rows.Count > 0)
                return bnv.Rows[0][0].ToString();
            else
                return null;
        }

        

        public bool bnvThemMoiNV(string maNV, string tenNV, string tenDN, string matKhau, DateTime ngaySinh, 
            string gioiTinh, string diaChi, string dienThoai, string maHocHam, string maHocVi, 
            string maNghiepVu, string maKhoa, Image imgAnh)
        {
            byte[] mbyteAnh = HinhAnh.haChuyenAnhSangByte(imgAnh);
            SqlParameter[] msp = new SqlParameter[13];

            msp[0] = new SqlParameter("@MaNV", maNV);
            msp[1] = new SqlParameter("@TenNV", tenNV);
            msp[2] = new SqlParameter("@TenDN", tenDN);
            msp[3] = new SqlParameter("@MatKhau", matKhau);
            msp[4] = new SqlParameter("@NgaySinh", ngaySinh);
            msp[5] = new SqlParameter("@GioiTinh", gioiTinh);
            msp[6] = new SqlParameter("@DiaChi", diaChi);
            msp[7] = new SqlParameter("@DienThoai", dienThoai);
            msp[8] = new SqlParameter("@MaHocHam", maHocHam);
            msp[9] = new SqlParameter("@MaHocVi", maHocVi);
            msp[10] = new SqlParameter("@MaNghiepVu", maNghiepVu);
            msp[11] = new SqlParameter("@MaKhoa", maKhoa);
            msp[12] = new SqlParameter("@Anh", SqlDbType.Image, mbyteAnh.Length);
            msp[12].Value = mbyteAnh;
            
            if (cdStoreProceduce("spThemNhanVien", msp))
                return true;
            else
                return false;
        }

        public static BangNhanVien bnvLayThongTinNV()
        {
            return new BangNhanVien("select nv.Anh, nv.MaNV, nv.HoTenNV, nv.TenDangNhap, nv.NgaySinh, nv.GioiTinh, nv.DiaChi, nv.DienThoai, hh.TenHocHam, hv.TenHocVi, nvu.TenNghiepVu, k.TenKhoa"
                                    +" from NHANVIEN nv inner join HOCHAM hh"
                                    +" on  hh.MaHocHam = nv.MaHocHam inner join HOCVI hv"
                                    +" on hv.MaHocVi = nv.MaHocVi inner join NGHIEPVU nvu"
                                    +" on nvu.MaNghiepVu = nv.MaNghiepVu inner join KHOA k"
                                    +" on k.MaKhoa = nv.MaKhoa;");
        }

        public static BangNhanVien bnvLayThongTinNV(string maNV)
        {
            return new BangNhanVien("select nv.Anh, nv.MaNV, nv.HoTenNV, nv.TenDangNhap, nv.MatKhau, nv.NgaySinh, nv.GioiTinh, nv.DiaChi, nv.DienThoai, hh.MaHocHam, hv.MaHocVi, nvu.MaNghiepVu, k.MaKhoa"
                                    +" from NHANVIEN nv inner join HOCHAM hh"
                                    +" on  hh.MaHocHam = nv.MaHocHam inner join HOCVI hv"
                                    +" on hv.MaHocVi = nv.MaHocVi inner join NGHIEPVU nvu"
                                    +" on nvu.MaNghiepVu = nv.MaNghiepVu inner join KHOA k"
                                    +" on k.MaKhoa = nv.MaKhoa"
                                    +" where nv.MaNV = '"+maNV+"';");
        }

        public bool bnvSuaThongTinNV(string maNV, string tenNV, string tenDN, string matKhau, DateTime ngaySinh, string gioiTinh,
            string diaChi, string dienThoai, string maHocHam, string maHocVi, string maNghiepVu, string maKhoa, Image imgAnh)
        {
            byte[] mbyteAnh = HinhAnh.haChuyenAnhSangByte(imgAnh);
            SqlParameter[] msp = new SqlParameter[13];

            msp[0] = new SqlParameter("@MaNV", maNV);
            msp[1] = new SqlParameter("@TenNV", tenNV);
            msp[2] = new SqlParameter("@TenDN", tenDN);
            msp[3] = new SqlParameter("@MatKhau", matKhau);
            msp[4] = new SqlParameter("@NgaySinh", ngaySinh);
            msp[5] = new SqlParameter("@GioiTinh", gioiTinh);
            msp[6] = new SqlParameter("@DiaChi", diaChi);
            msp[7] = new SqlParameter("@DienThoai", dienThoai);
            msp[8] = new SqlParameter("@MaHocHam", maHocHam);
            msp[9] = new SqlParameter("@MaHocVi", maHocVi);
            msp[10] = new SqlParameter("@MaNghiepVu", maNghiepVu);
            msp[11] = new SqlParameter("@MaKhoa", maKhoa);
            msp[12] = new SqlParameter("@Anh", SqlDbType.Image, mbyteAnh.Length);
            msp[12].Value = mbyteAnh;

            if (cdStoreProceduce("spSuaNhanVien", msp))
                return true;
            else
                return false;
        }

        public bool bnvXoaNhanVien(string maNV)
        {
            SqlParameter[] msp = new SqlParameter[1];
            msp[0] = new SqlParameter("@MaNV", maNV);
            if (cdStoreProceduce("spXoaNhanVien", msp))
                return true;
            else
                return false;
        }

        public static BangNhanVien bnvLayMaTenVaKhoaNV()
        {
            return new BangNhanVien("select NV.MaNV, NV.HoTenNV,K.TenKhoa from NHANVIEN NV inner join KHOA K on K.MaKhoa = NV.MaKhoa;");
        }
    }
}
